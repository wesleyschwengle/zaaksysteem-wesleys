package Zaaksysteem::API::v1::Serializer::Reader::Map;

use Moose;

with 'Zaaksysteem::API::v1::Serializer::DispatchReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::Map - Map readers, for the Map Module

=head1 DESCRIPTION

This serializer encodes various Map Objects into a valid APIv1 JSON structure.

=head1 OBJECT TYPES

The following object types will be converted via this reader:

=head1 METHODS

=head2 dispatch_map

The dispatcher for various Map Objects

=head2 ol_settings

OpenLayer settings

=head3 ol_layer_wms

OpenLayers Layer WMS

=cut

sub dispatch_map {
    return (
        'Zaaksysteem::Backend::Sysin::Modules::Map::OL' => sub { __PACKAGE__->read_ol_settings(@_) },
        'Zaaksysteem::Backend::Sysin::Modules::Map::OL::LayerWMS' => sub { __PACKAGE__->read_ol_wms_layer(@_) },
    );
}

=head2 read_ol_wms_layer

Return the contents of a OpenLayer WMS layer

=cut

sub read_ol_wms_layer {
    my ($class, $serializer, $object) = @_;

    return {
        type => 'ol_layer_wms',
        reference => undef,
        instance => {
            map { $_ => $object->$_ } qw/id url label active feature_info_xpath/
        }
    };
}

=head2 read_ol_settings

Return the contents of a OpenLayer WMS layer

=cut

sub read_ol_settings {
    my ($class, $serializer, $object) = @_;

    return {
        type => 'ol_settings',
        reference => undef,
        instance => {
            (map { $_ => $object->$_ } qw/map_center/),
            wms_layers => [ map({ $serializer->read($_) } @{ $object->wms_layers }) ],
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
