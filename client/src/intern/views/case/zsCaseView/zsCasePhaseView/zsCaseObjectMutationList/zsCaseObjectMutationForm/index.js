import angular from 'angular';
import vormFieldsetModule from './../../../../../../../shared/vorm/vormFieldset';
import vormValidatorModule from './../../../../../../../shared/vorm/util/vormValidator';
import composedReducerModule from './../../../../../../../shared/api/resource/composedReducer';
import resourceModule from './../../../../../../../shared/api/resource';
import attrToVorm from './../../../../../../../shared/zs/vorm/attrToVorm';
import caseAttrTemplateCompilerModule from './../../../../../../../shared/case/caseAttrTemplateCompiler';
import seamlessImmutable from 'seamless-immutable';
import identity from 'lodash/identity';
import get from 'lodash/get';
import assign from 'lodash/assign';
import first from 'lodash/head';
import pickBy from 'lodash/pickBy';
import find from 'lodash/find';
import mapValues from 'lodash/mapValues';
import omit from 'lodash/omit';
import isArray from 'lodash/isArray';
import template from './template.html';
import './styles.scss';

export default
	angular.module('zsCaseObjectMutationForm', [
		vormFieldsetModule,
		composedReducerModule,
		resourceModule,
		vormValidatorModule,
		caseAttrTemplateCompilerModule
	])
		.directive('zsCaseObjectMutationForm', [ '$animate', 'composedReducer', 'resource', 'vormValidator', 'caseAttrTemplateCompiler', ( $animate, composedReducer, resource, vormValidator, caseAttrTemplateCompiler ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					objectTypeResource: '&',
					objectTypeLabel: '&',
					mutationType: '&',
					mutationVerb: '@',
					objectId: '&',
					defaults: '&',
					onSubmit: '&',
					onCancel: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this,
						values = seamlessImmutable({}),
						objectResource,
						actionReducer,
						fieldReducer,
						validityReducer;

					let convertAttr = ( attribute, val ) => {

						let activeValues =
							get(attribute.data, 'values', []).filter( option => {
								return option.active || option.value === val;
							});

						return {
							id: attribute.attribute_id,
							label: attribute.label || attribute.attribute_label,
							type: attribute.attribute_type,
							required: attribute.required,
							help: '',
							magic_string: attribute.name.replace('attribute.', ''),
							limit_values:
								attribute.attribute_type === 'subject'
								|| attribute.multiple_values ?
								-1
								: 1,
							values: activeValues
						};
					};

					let transformValues = ( type, vals ) => {

						let reduce = ( field, value ) => {

							return field[type] ?
								field[type].reduce(( current, reducer ) => {
									return reducer(current);
								}, value)
								: value;

						};

						return pickBy(
							mapValues(
								vals,
								( value, key ) => {

									let field = find(fieldReducer.data(), { name: key }),
										val = value;

									if (!field) {
										return null;
									}

									val = isArray(val) && field.template !== 'checkbox-list' ? val.map(v => reduce(field, v)) : reduce(field, val);

									if (field.limit === 1 && field.template !== 'checkbox-list' && isArray(val)) {
										val = first(val);
									}

									return val;

								}
							),
							identity
						);

					};

					actionReducer = composedReducer( { scope }, ( ) => ctrl.mutationVerb)
						.reduce( ( verb ) => {

							return [
								{
									label: 'Annuleren',
									click: ( ) => {
										ctrl.onCancel();
									}
								},
								{
									label: verb,
									type: 'submit',
									click: ( ) => {

										let vals = transformValues(
											'parsers',
											omit(values, '$object')
										);

										ctrl.onSubmit({
											$values: vals,
											$object: values.$object
										});
									},
									classes: {
										'btn-primary': true
									}
								}
							];

						});

					fieldReducer = composedReducer( { scope }, ctrl.objectTypeResource(), ctrl.mutationType, ctrl.objectId, ( ) => values)
						.reduce( ( objectType, type, objectId, vals ) => {

							let fields = [];

							if (type === 'relate' || type === 'update' || type === 'delete') {
								fields = fields.concat({
									name: '$object',
									label: ctrl.objectLabel,
									template: {
										inherits: 'object-suggest',
										display: ( ) => {

											return angular.element(
												`<div class="file-list-item object-mutation-form-item">
													<a ng-href="/object/{{delegate.value.id}}" target="_blank">
														<span>{{delegate.value.label}}</span>
														<zs-icon icon-type="launch"></zs-icon>
													</a>
													<zs-spinner class="spinner-tiny" is-loading="vm.templateData().isLoading()"></zs-spinner>
												</div>`
											);

										}
									},
									data: {
										objectType: objectType.values.prefix,
										isLoading: ( ) => objectResource.state() === 'pending'
									},
									required: true,
									disabled: !!objectId
								});
							}

							fields = fields.concat(
								get(objectType, 'values.attributes', [])
									.map( attribute => {

										let attrName = attribute.name.replace('attribute.', ''),
											attr = attrToVorm(convertAttr(attribute, vals[attrName])),
											disabled =
												(type === 'delete' || type === 'relate'),
											required = attribute.required && !disabled,
											when = true,
											config;

										if (type === 'update' || type === 'delete' || type === 'relate') {
											when = ( ) => {
												return values.$object && objectResource.state() === 'resolved';
											};
										}
										
										config = assign(
											{},
											attr,
											{
												disabled,
												required,
												when
											}
										);

										if (attr.type === 'file') {
											config = assign(
												{},
												config,
												{
													template: 'file',
													data: assign(
														{},
														config.data,
														{
															target: '/filestore/upload',
															transform: [ '$file', '$data', ( file, data ) => {

																return data.result[0];

															}],
															display: ( file ) => {
																return file.original_name;
															}
														}
													)
												}
											);
										}

										return config;

									})
									.filter(identity)
								);
							
							return seamlessImmutable(fields).asMutable({ deep: true });

						});

					objectResource = resource(
						( ) => {

							let objectId = ctrl.objectId() ? ctrl.objectId() : get(values, '$object.id');

							return objectId ?
								`/api/object/${objectId}`
								: null;
						},
						{ scope }
					)
						.reduce(( requestOptions, data ) => first(data));

					if (ctrl.objectId()) {
						objectResource.onUpdate( ( ) => {

							values = values.merge({ $object: objectResource.data() });

						});
					}

					composedReducer( { scope, mode: 'hot', waitUntilResolved: false }, objectResource, ctrl.objectTypeResource(), ctrl.defaults())
						.onUpdate( ( ) => {

							let object = objectResource.data(),
								objectType = ctrl.objectTypeResource().data(),
								defaults = ctrl.defaults(),
								vals = {};

							if (object && objectType) {
								vals = omit(object.values, 'date_created', 'date_modified');
							}

							values = values.merge(
								transformValues(
									'formatters',
									assign(vals, defaults)
								)
							);
						});

					validityReducer = composedReducer( { scope }, fieldReducer, ( ) => values )
						.reduce( ( fields = [], vals = {} ) => {

							return vormValidator(fields, vals);

						});

					ctrl.getCompiler = ( ) => caseAttrTemplateCompiler;

					ctrl.getActions = actionReducer.data;

					ctrl.getFields = fieldReducer.data;

					ctrl.isValid = ( ) => get(validityReducer.data(), 'valid');

					ctrl.handleChange = ( name, value ) => {

						if (name === '$object' && !value) {
							values = seamlessImmutable({});
						} else {
							values = values.merge({ [name]: value });
						}

					};

					ctrl.handleClick = ( action ) => {
						action.click();
					};

					ctrl.getValues = ( ) => values;

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
