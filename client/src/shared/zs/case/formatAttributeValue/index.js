import keyBy from 'lodash/keyBy';
import identity from 'lodash/identity';
import mapValues from 'lodash/mapValues';
import isArray from 'lodash/isArray';
import compact from 'lodash/compact';

export default ( attribute, source ) => {

	let value = source;

	switch (attribute.type) {
		case 'checkbox':
		if (!isArray(value)) {
			value = [ value ];
		}

		value = mapValues(
			keyBy(compact(value), identity),
			( ) => true
		);

		if (Object.keys(value).length === 0) {
			value = null;
		}
		break;

		case 'date':
		if (value) {
			value = new Date(value);
		}
		break;
	}

	return value;

};
