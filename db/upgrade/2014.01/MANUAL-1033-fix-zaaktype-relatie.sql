BEGIN;
COPY (SELECT * FROM zaaktype_relatie WHERE relatie_zaaktype_id IS NULL) TO STDOUT;
DELETE FROM zaaktype_relatie WHERE relatie_zaaktype_id IS NULL;
ALTER TABLE zaaktype_relatie ALTER relatie_zaaktype_id SET NOT NULL;
COMMIT;
