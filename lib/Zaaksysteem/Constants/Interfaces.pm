package Zaaksysteem::Constants::Interfaces;
use warnings;
use strict;

=head1 NAME

Zaaksysteem::Constants::Interfaces - Constants for Interfaces or Koppelprofielen

=head1 DESCRIPTION

=head1 SYNOPSIS

    use Zaaksysteem::Constants::Users qw(ALLOWED_INTERFACES);

=cut

require Exporter;
our @ISA        = qw(Exporter);
our @EXPORT_OK  = qw(ALLOWED_INTERFACES);
our %EXPORT_TAGS  = ( all => \@EXPORT_OK );

use constant ALLOWED_INTERFACES => qw(
    email
    kcc
    next2know
    overheidio
    qmatic
    stuf_dcr
    stufconfig
    supersaas
    xential
);


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
