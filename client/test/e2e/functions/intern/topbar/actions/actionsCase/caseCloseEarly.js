import openAction from './../openAction';

export default ( result ) => {

	let form = $('zs-case-admin-view form');

	openAction('Vroegtijdig afhandelen');

	$('[data-name="reason"] input').sendKeys('Reason');
	$(`[value="${result}"]`).click();

	form.submit();

};
