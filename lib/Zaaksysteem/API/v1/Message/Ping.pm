package Zaaksysteem::API::v1::Message::Ping;

use Moose;

use Crypt::OpenSSL::Random qw[random_pseudo_bytes];

extends 'Zaaksysteem::API::v1::Message';

=head1 NAME

Zaaksysteem::API::v1::Message::Ping - Model for ping messages for APIv1

=head1 DESCRIPTION

This message is meant for testing purposes of external systems. When receiving
a message of this type, the external system is to respond with a pong message
containing the same payload.

The payload is a randomly generated string of hexadecimal numbers, which
serves a verification of delivery method.

=head1 METHODS

=head2 field_hash

Override for L<Zaaksysteem::API::v1::Message/field_hash> that provides
message type and payload information for the ping message.

    {
        type => 'ping',
        payload => '23FA31B9'
    }

=cut

override field_hash => sub {
    my $self = shift;

    return {
        type => 'ping',
        payload => unpack('H*', random_pseudo_bytes(16))
    }
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
