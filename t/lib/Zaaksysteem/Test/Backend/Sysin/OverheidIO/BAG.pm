package Zaaksysteem::Test::Backend::Sysin::OverheidIO::BAG;

=head1 NAME

Zaaksysteem::Test::Backend::Sysin::OverheidIO::BAG - Test OverheidIO model for BAG

=head2 SYNOPSIS

    prove -l :: Zaaksysteem::Test::Backend::Sysin::OverheidIO::BAG

=cut

use Zaaksysteem::Test;
use Zaaksysteem::Backend::Sysin::OverheidIO::BAG;
use Zaaksysteem::Backend::Sysin::OverheidIO::Model;
use URI;
use IO::All;

sub test_setup {
    my $test = shift;

    my $test_method = $test->test_report->current_method->name;

    if ('test_overheid_io_live' eq $test_method
        && !$ENV{ZS_OVERHEID_IO_API_KEY})
    {
        $test->test_skip("Skipping $test_method: set ZS_OVERHEID_IO_API_KEY");
    }
}

sub _create_overheid_io_ok {
    my $options = {@_};
    my $model   = Zaaksysteem::Backend::Sysin::OverheidIO::BAG->new(
        $options->{base_uri} ?
            (base_uri => URI->new($options->{base_uri})) : (),
        key => $options->{key} // 'zs-testsuite',
    );
    isa_ok($model, 'Zaaksysteem::Backend::Sysin::OverheidIO::BAG');
    return $model;
}

sub test_overheid_io_live {

    my $oio = _create_overheid_io_ok(
        key => $ENV{ZS_OVERHEID_IO_API_KEY}
    );

    my $expected_data = {
        openbareruimtenaam   => "Muiderstraat",
        huisnummer           => "1",
        huisnummertoevoeging => "",
        huisletter           => "",
        woonplaatsnaam       => "Amsterdam",
        gemeentenaam         => "Amsterdam",
        postcode             => "1011PZ",
        provincienaam        => "Noord-Holland",
        url                  => "1011pz-muiderstraat-1",
        _links => { self => { href => "/api/bag/1011pz-muiderstraat-1" }, },
    };

    {
        my $answer        = $oio->search("Muiderstraat*");
        my $address       = $answer->{_embedded}{adres}[0];

        my @keys = sort keys(%{$address});
        my @expected_keys = sort keys(%{$expected_data});

        if(!cmp_deeply(\@keys, \@expected_keys, "Address search results looks sane")) {
            diag explain \@keys;
        }
    }

    {
        my $answer = $oio->search("Muiderstraat",
            filter => {
                huisnummer => 1,
                postcode   => '1011PZ'
            }
        );
        my $address = $answer->{_embedded}{adres}[0];

        is($answer->{totalItemCount}, 1, "One address found");
        cmp_deeply($address, $expected_data, "Address search result matches expected data");
    }

    {
        my $answer = $oio->search("Muiderstraat",
            filter => {
                huisnummer => 1,
                postcode   => '1095EW'
            }
        );
        is($answer->{totalItemCount}, 0, "No address found, filter denies");
    }

    {
        my $answer = $oio->search("Muiderstraa");
        is($answer->{totalItemCount}, 0, "No address found, exact search");
    }

    {
        my $answer = $oio->search(
            undef,
            filter => {
                huisnummer => "1",
                postcode   => "1011PZ",
            }
        );
        if (is($answer->{totalItemCount}, 1, "One address found by huisnummer/postcode filters")) {
            my $address       = $answer->{_embedded}{adres}[0];
            is_deeply($address, $expected_data,
                "Address search result matches expected data");
        }
    }

}

sub test_overheid_io {

    my $oio = _create_overheid_io_ok();
    no warnings qw(redefine once);
    use HTTP::Response;
    my $answer = io->catfile('t/data/overheid.io/search_bag.json')->slurp;
    local *LWP::UserAgent::request = sub {
        my $self = shift;
        return HTTP::Response->new(200, undef, undef, $answer);
    };
    use warnings;

    $answer = $oio->search("foo");

    my $address = $answer->{_embedded}{adres}[0];

    my $expected_data = {
        openbareruimtenaam   => "Muiderstraat",
        huisnummer           => 1,
        huisnummertoevoeging => '',
        huisletter           => '',
        postcode             => "1011PZ",
        woonplaatsnaam       => 'Amsterdam',
        url                  => '1011pz-muiderstraat-1',
        _links => { self => { href => "/api/bag/1011pz-muiderstraat-1" } },
    };

    cmp_deeply($address, $expected_data, "Muiderstraat found");
}

sub test_overheid_io_uri {
    my $oio = _create_overheid_io_ok();

    _test_overheid_io_uri_query("Simple search", $oio, "foo");
    _test_overheid_io_uri_query("Filter search",
        $oio, "foo", filter => { foo => 'bar' });

}

sub _test_overheid_io_uri_query {
    my ($test_name, $model, $search, %params) = @_;

    my $calling_uri;
    no warnings qw(redefine once);
    local *Zaaksysteem::Backend::Sysin::OverheidIO::Model::_call_overheid_io = sub {
        my $self = shift;
        $calling_uri = shift;
    };
    use warnings;

    $model->search($search, %params);

    my @query = $calling_uri->query_form;
    my @expected_query = ("size" => 30,);

    foreach (keys %{ $params{filter} }) {
        push(@expected_query, "filters[$_]" => $params{filter}{$_});
    }

    foreach (@{ $model->fieldnames }) {
        push(@expected_query, 'fields[]' => $_);
    }

    foreach (@{ $model->queryfields }) {
        push(@expected_query, 'queryfields[]' => $_);
    }
    push(@expected_query, query => "foo");

    @query          = sort { $a cmp $b } @query;
    @expected_query = sort { $a cmp $b } @expected_query;

    cmp_deeply(\@query, \@expected_query, "Correct query params: $test_name");
    is($calling_uri->path, "/api/bag", "API path is correct: $test_name");
}

1;

__END__

=head1 NAME

Zaaksysteem::Test::Backend::Sysin::OverheidIO::Model - A OverheidIO test package

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
