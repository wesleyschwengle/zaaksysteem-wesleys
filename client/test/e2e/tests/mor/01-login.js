import login from './../../functions/common/auth/login';
import logout from './../../functions/common/auth/logout';

describe('when logging in', ( ) => {

	beforeAll(( ) => {

		browser.get('/mor/');

	});

	describe('with valid credentials', ( ) => {

		it('should redirect to the application', ( ) => {

			expect(browser.getCurrentUrl()).toMatch(/mor/);

		});

	});

	describe('with invalid credentials', ( ) => {

		it('should redirect to the login form', ( ) => {

			logout();

			browser.get('/mor');

			expect(browser.getCurrentUrl()).toMatch('/auth/login');

		});

	});

	afterAll(( ) => {

		login();

		browser.get('/intern/');

	});

});
