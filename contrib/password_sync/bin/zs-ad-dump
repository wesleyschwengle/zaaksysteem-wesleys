#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
#use FindBin;

use Net::LDAP;
#use XML::Simple;
use XML::Dumper;
use WWW::Curl::Form;
use WWW::Curl::Easy;

use constant    LDAP_CUSTOMER       => 'Customer';
use constant    LDAP_HOST           => 'localhost';
use constant    LDAP_USER           => 'mintlab';
use constant    LDAP_PASS           => 'password';
use constant    LDAP_BASE           => 'OU=Users,O=NEW,dc=example,dc=com';

use constant    PASSWD_FILE         => '/opt/var/ad_passwords';
#use constant    POST_URL            => 'http://dev.zaaksysteem.nl:3000/api/users';
use constant    POST_URL            => 'https://bla.example.com/api/users';
use constant    POST_API_KEY        => 'abcdeawerawer@#2323';

use constant    LDAP_COLUMNS_EXTRA  => [qw/
    accountExpires
    department
    company
    instanceType
    l
    lastKnownParent
    mailNickname
    name
    physicalDeliveryOfficeName
    postOfficeBox
    postalCode
    streetAddress
    title
    userPrincipalName
/];

use constant    LDAP_COLUMNS        => [qw/
    cn
    sn
    displayName
    givenName
    mail
    telephoneNumber
/];

use constant    LDAP_FUTURE_COLUMNS => [
    qw/
        memberOf
    /
];

my $ldap = Net::LDAP->new( LDAP_HOST ) or die "$@";

$ldap->bind(LDAP_USER, password => LDAP_PASS);

sub password {
    my ($username)  = @_;

    my $entries = $ldap->search( # perform a search
        base   => LDAP_BASE,
        filter => '(&(userPrincipalName=' . $username . '@*))'
    );

    return $entries;
}

sub full {
    my $entries = $ldap->search( # perform a search
        base   => LDAP_BASE,
        filter => "(&(cn=*)(objectClass=user))",
        scope  => 'subtree',
    );

    return $entries;
}

sub prepare_entries {
    my ($entries, $passwords)   = @_;
    my $xml_hash    = {};

    my $columns         = LDAP_COLUMNS;
    my $extra_columns   = LDAP_COLUMNS_EXTRA;

    $columns = [ @{ $columns }, @{ $extra_columns } ];

    for my $entry ($entries->entries) {
        my $username    = lc($entry->get_value('userPrincipalName'));
        $username       =~ s/\@.*$//;

        ### Prevent Disabled users/Muiden/Naarden/Algemene Users
        next if (
            $entry->dn =~ /ou=Disabled/i ||
            $entry->dn =~ /ou=Muiden/i ||
            $entry->dn =~ /ou=Naarden/i ||
            $entry->dn =~ /ou=Algemene Users/i
        );

        my $rv          = {};

        for my $column (@{ $columns }) {
            $rv->{$column}  = $entry->get_value($column);
        }

        if ($passwords->{$username}) {
            $rv->{userPassword} = $passwords->{$username};
        }

        $rv->{cn}   = $username;

        $xml_hash->{$username} = $rv;
    }

    print "Number of entries: " . $entries->count . "\n";

    my $customer    = LDAP_CUSTOMER;
    my $xml         = {
        $customer   => $xml_hash
    };

    return $xml;
}

### There could be duplicates, but by the nature of reading this password
### file, it will automatically overwrite old values
sub retrieve_passwords {
    return {} unless (-f PASSWD_FILE);

    open (my $FH, '<' . PASSWD_FILE) or
        die('Problems reading PASSWD_FILE:' . PASSWD_FILE);

    my $rv      = {};
    while (<$FH>) {
        chomp;
        my ($username, $password) = split(/:/);

        $rv->{lc($username)} = $password;
    }

    close($FH);

    ### DELETE PASSWORD FILE ?
    return $rv;
}

sub sync_entries {
    my $hash    = shift;
    my $response_body;

    #print Dumper($hash);
    my $xs      = XML::Dumper->new;

    my $xml     = $xs->pl2xml($hash);
    #print $xml;
    #return;

    my $params  = {
        api_key     => POST_API_KEY,
        component   => 'sync_users',
        message     => $xml,
    };

    #my $got_xml = $xml;
    my $curl    = WWW::Curl::Easy->new;
    my $curlf   = WWW::Curl::Form->new;

    $curl->setopt(CURLOPT_HEADER,1);
    $curl->setopt(CURLOPT_URL, POST_URL);
    $curl->setopt(CURLOPT_WRITEDATA,\$response_body);

    $curlf->formadd($_, $params->{$_}) for keys %{ $params };

    $curl->setopt(CURLOPT_HTTPPOST, $curlf);

    my $retcode = $curl->perform;
    print("An error happened: $retcode ".$curl->strerror($retcode)."
        ".$curl->errbuf."\n") if $retcode;
}


sub main {
    if (
        @ARGV < 1 ||
        $ARGV[0] !~ /password|full/ ||
        $ARGV[0] eq 'password' && !$ARGV[1]
    ) {
        print "USAGE: $0 full|password [username]\n";
        exit;
    }

    my $passwords   = retrieve_passwords;
    #my $entries     = password('dummyzaaksysteem');
    my $entries     = full;

    my $ldap_hash   = prepare_entries($entries, $passwords);

    sync_entries($ldap_hash);
}

main;

1;
