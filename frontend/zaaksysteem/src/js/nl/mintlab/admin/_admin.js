/*global angular*/
(function () {
    "use strict";

    angular.module('Zaaksysteem.admin', [ 
		'Zaaksysteem.admin.casetype',
		'Zaaksysteem.admin.objecttype',
		'Zaaksysteem.admin.instances'
    ]);

})();
