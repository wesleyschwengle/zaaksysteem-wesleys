import angular from 'angular';
import template from './template.html';
import startsWith from 'lodash/startsWith';

export default
	angular.module('zsBtwDisplay', [
	])
		.directive('zsBtwDisplay', [ 'currencyFilter', ( currencyFilter ) => {

			let multipliers = {
				in: 1.19,
				in21: 1.21,
				in6: 1.06,
				ex: 1 / 1.19,
				ex6: 1 / 1.06,
				ex21: 1 / 1.21
			};

			return {
				restrict: 'E',
				template,
				scope: {
					value: '&',
					btwType: '&'
				},
				bindToController: true,
				controller: [ function ( ) {

					let ctrl = this;

					ctrl.getBtwValue = ( ) => {

						let amount = null,
							val = ctrl.value();

						if (isNaN(val) || val === null) {
							amount = '';
						} else {
							amount = currencyFilter(Math.round(val * multipliers[ctrl.btwType()] * 100) / 100);
						}

						return amount;

					};

					ctrl.getBtwDescription = ( ) => {
						return `
							${startsWith(ctrl.btwType(), 'in') ? 'incl.' : 'excl.'}
							btw`;
					};

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
