package Zaaksysteem::Controller::API::SuperSaaS;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Exception;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head2 index

Base controller for SuperSaaS API-calls

=cut

sub index : Local : ZAPI {
    my ($self, $c) = @_;

    eval {
        my $supersaas = $c->model('DB::Interface')->search_active({ module => 'supersaas' })->first
            or die "need supersaas interface";

        my $params = $c->req->params;
        my $action = $params->{action};

        my $result = $supersaas->process_trigger($action, $params);

        die "need result array" unless $result && ref($result) eq 'ARRAY';

        $c->stash->{zapi} = $result;
    };

    if ($@) {
        $c->log->error("supersaas error: $@");

        my $type = $@ =~ m|^no supersaas interface was configured for case_type_id|
            ? 'supersaas/config_missing'
            : 'supersaas/unknown';

        $c->stash->{zapi} = Zaaksysteem::ZAPI::Error->new(
            type             => $type,
            messages         => []
        );
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
