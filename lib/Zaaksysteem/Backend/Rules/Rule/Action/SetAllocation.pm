package Zaaksysteem::Backend::Rules::Rule::Action::SetAllocation;
use Moose;
use namespace::autoclean;

use Zaaksysteem::Tools;

with 'Zaaksysteem::Backend::Rules::Rule::Action';

=head1 NAME

Zaaksysteem::Backend::Rules::Rule::Action::SetAllocation - Set allocation rules

=head1 SYNOPSIS

=head1 DESCRIPTION

This specific action will allocate the case to a given department/role

=head1 ATTRIBUTES


=head2 attribute_name

The attribute name for this action is not required. See also L<Zaaksysteem::Backend::Rules::Rule::Action>

=cut

has '+attribute_name' => ( isa => 'Any', required => 0 );

=head2 group_id

The group ID which will be used to allocate the case to

=cut

has group_id => (
    is       => 'rw',
    isa      => 'Int',
    required => 1,
);

=head2 role_id

The role ID which will be used to allocate the case to

=cut

has role_id => (
    is       => 'rw',
    isa      => 'Int',
    required => 1,
);

=head2 _data_attributes

Override thes _data_attributes set by L<Zaaksysteem::Backend::Rules::Rule::Action>

=cut

has '+_data_attributes' => (
    default => sub { return [qw/role_id group_id/]; }
);


=head2 integrity_verified

For now return always return true, but we may want to skip the rule if the group/role does not exist on the platform

=cut

sub integrity_verified { return 1; }

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
