package Zaaksysteem::Schema::AlternativeAuthenticationActivationLink;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::AlternativeAuthenticationActivationLink

=cut

__PACKAGE__->table("alternative_authentication_activation_link");

=head1 ACCESSORS

=head2 token

  data_type: 'text'
  is_nullable: 0

=head2 subject_id

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 0
  size: 16

=head2 expires

  data_type: 'timestamp'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "token",
  { data_type => "text", is_nullable => 0 },
  "subject_id",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 0, size => 16 },
  "expires",
  { data_type => "timestamp", is_nullable => 0 },
);
__PACKAGE__->set_primary_key("token");

=head1 RELATIONS

=head2 subject_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Subject>

=cut

__PACKAGE__->belongs_to(
  "subject_id",
  "Zaaksysteem::Schema::Subject",
  { uuid => "subject_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2017-01-26 14:44:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Tv4pftZLay7jEndR/KaR5Q


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
