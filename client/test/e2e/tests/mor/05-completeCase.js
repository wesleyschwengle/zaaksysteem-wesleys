describe('when completing a case', ( ) => {

	beforeAll(( ) => {

		browser.get('/mor');

		let caseItem = element.all(by.css('case-list-item-list-item')).first();
		
		caseItem.click();

		let button = element.all(by.css('.case-detail-view__button-container button'));
		
		button.click();

	});

	it('should display at least one result option in the completion form', ( ) => {

		let resultItem = element(by.css('vorm-radio-group'));
		
		expect(resultItem.isPresent()).toBeTruthy();

	});

	it('should display one afhandelen button in the completion form that should be disabled when the form is pristine', ( ) => {

		let button = element(by.css('case-complete-view .case-detail-view__button-container button'));
		
		expect(button.isPresent()).toBeTruthy();

		expect(button.isEnabled()).toBe(false);

	});

	it('should change classes when a result option is selected', ( ) => {

		let resultItem = element.all(by.css('.vorm-field-item .value-list .value-item label')).first();

		resultItem.click();

		expect(resultItem.getAttribute('class')).toContain('active');

	});

	it('should display one afhandelen button in the completion form that should not be disabled when all options are pressed', ( ) => {

		let resultItem = element.all(by.css('.vorm-field-item .value-list .value-item label')).first();

		resultItem.click();

		let button = element(by.css('case-complete-view .case-detail-view__button-container button'));
		
		expect(button.isEnabled()).toBe(true);

	});

	it('should close the caseCompletion screen after clicking the afhandelen button which leads to a successfull closing of a case', ( ) => {

		let caseListView = element.all(by.css('case-list-view'));
		
		let button = element.all(by.css('.case-detail-view__button-container button'));

		browser.actions().mouseMove(button).click();
		
		expect(caseListView.count()).toEqual(1);

	});

	afterAll(( ) => {

		browser.get('/intern');

	});

});
