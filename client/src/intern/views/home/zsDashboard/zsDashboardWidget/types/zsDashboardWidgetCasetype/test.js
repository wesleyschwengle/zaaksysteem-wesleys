import angular from 'angular';
import 'angular-mocks';
import zsDashboardWidgetCasetype from './index.js';
import apiCacherModule from './../../../../../../../shared/api/cacher';

describe('zsDashboardWidgetCasetype', ( ) => {

	let $rootScope,
		$compile,
		$httpBackend,
		apiCacher,
		scope,
		el,
		ctrl;

	beforeEach(angular.mock.module([ '$provide', ( $provide ) => {

		$provide.constant('HOME', '/');

	}]));

	beforeEach(angular.mock.module(zsDashboardWidgetCasetype, apiCacherModule));

	beforeEach(angular.mock.inject([ '$rootScope', '$compile', '$httpBackend', 'apiCacher', ( ...rest ) => {

		[ $rootScope, $compile, $httpBackend, apiCacher ] = rest;

		apiCacher.clear();

		el = angular.element('<zs-dashboard-widget-casetype/>');
		
		scope = $rootScope.$new();

		$compile(el)(scope);

		ctrl = el.controller('zsDashboardWidgetCasetype');

	}]));

	beforeEach( ( ) => {

		$httpBackend.expectGET('/api/v1/dashboard/favourite/casetype')
			.respond([]);

	});
	
	it('should have a controller', () => {

		expect(ctrl).toBeDefined();

	});

	afterEach(( ) => {

		$httpBackend.flush();

		apiCacher.clear();

		scope.$destroy();

	});
});
