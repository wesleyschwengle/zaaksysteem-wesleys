#!/bin/bash

db=zaaksysteem
hostname=vagrant.zaaksysteem.nl

while getopts "hd:f:u:n:" name
do
    case $name in
        h) usage;;
        d) db=$OPTARG;;
        f) file=$OPTARG;;
        n) new_file=$OPTARG;;
        u) hostname=$OPTARG;;
    esac
done
shift $((OPTIND - 1))

INTERFACE=$(echo $(psql -t $db -c "SELECT uuid FROM interface WHERE module='xential' order by id desc limit 1"))
INTERFACE_ID=$(psql -t $db -c "SELECT id FROM interface WHERE uuid = '$INTERFACE'")

TID=$(echo $(psql -t $db -c "SELECT uuid, processor_params from transaction WHERE interface_id = $INTERFACE_ID order by id desc" | grep -v _process_request_edit_file| awk '{print $1}'| head -1 ))

PROC_PARAMS=$(psql -t $db -c "SELECT processor_params from transaction WHERE interface_id = $INTERFACE_ID and uuid = '$TID'")

CID=$(echo $PROC_PARAMS | grep '"case":' | perl -pe 's/.*"case":(\d+).*/$1/');
# Empty CID means we processed the transaction already, for testing
# purposes we override the $CID
if [ -z "$CID" ]
then
    CASEUUID=$(echo $PROC_PARAMS | grep case_uuid | perl -pe 's/.*"case_uuid":(\w+).*/$1/')

else
    CASEUUID=$(echo $(psql -t $db -c "SELECT uuid from object_data where object_id = $CID and object_class = 'case'"))
fi

BASE_URL="https://$hostname/api/v1/sysin/interface/$INTERFACE/trigger/api_post_file?transaction_uuid=$TID&case_uuid=$CASEUUID"

if [ -n "$file" ]
then
    curl -k --form "upload=@$file"  $BASE_URL
fi

FILE_UUID=$(echo $(psql -t $db -c "SELECT uuid FROM filestore fs JOIN file f on f.filestore_id = fs.id and f.generator = 'xential' and f.active_version = true order by f.id desc limit 1"));

echo "Added file $FILE_UUID";

if [ -n "$new_file" ]
then
    curl -k --form "upload=@$new_file" "$BASE_URL&file_uuid=$FILE_UUID"
fi
