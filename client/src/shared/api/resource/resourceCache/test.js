import angular from 'angular';
import resourceCacheModule from '.';
import apiCacherModule from './../../cacher';

describe('resourceCache', ( ) => {

	let $rootScope,
		$httpBackend,
		requestOptions = { url: '/foo' },
		apiCacher,
		resourceCache,
		cache,
		storedData = { foo: 'bar' },
		responseData = { bar: 'foo' },
		cacheOptions;

	beforeEach(angular.mock.module(resourceCacheModule, apiCacherModule));

	beforeEach(angular.mock.inject([ '$rootScope', '$httpBackend', 'apiCacher', 'resourceCache', ( ...rest ) => {

		[ $rootScope, $httpBackend, apiCacher, resourceCache ] = rest;

	}]));

	describe('with cache enabled', ( ) => {
			
		let error = { error: 'foo' };

		it('should call the update listeners when the data changes', ( ) => {

			let spy = jasmine.createSpy('update'),
				removeFn;

			$httpBackend.expectGET(requestOptions.url)
				.respond(responseData);

			cache = resourceCache(requestOptions);

			removeFn = cache.onUpdate(spy);

			$httpBackend.flush();

			expect(spy).toHaveBeenCalled();

			spy.calls.reset();

			removeFn();

			apiCacher.store(requestOptions, storedData);

			expect(spy).not.toHaveBeenCalled();

		});

		describe('with every not defined', ( ) => {

			beforeEach( ( ) => {
				cacheOptions = { };
			});

			describe('without cache', ( ) => {

				describe('with a successful request', ( ) => {

					beforeEach( ( ) => {

						$httpBackend.expectGET(requestOptions.url)
							.respond(200, responseData);
							
						cache = resourceCache(requestOptions, cacheOptions);

					});

					it('should be pending and switch to resolved when fetched', ( ) => {

						expect(cache.state()).toBe('pending');

						$httpBackend.flush();

						expect(cache.state()).toBe('resolved');

					});

					it('should invoke the update listeners when the request is resolved', ( ) => {

						let spy = jasmine.createSpy('update');

						cache.onUpdate(spy);

						expect(spy).toHaveBeenCalled();

						spy.calls.reset();

						$httpBackend.flush();

						expect(spy).toHaveBeenCalled();

					});

					it('should fetch the data once', ( ) => {

						$httpBackend.flush(1);

						$httpBackend.verifyNoOutstandingRequest();
						$httpBackend.verifyNoOutstandingExpectation();

					});

					it('should return data from the cache', ( ) => {

						expect(cache.data()).toBeNull();

						$httpBackend.flush();

						expect(cache.data()).toEqual(responseData);

					});

					it('should update data when the cache is updated', ( ) => {

						$httpBackend.flush();

					});

					describe('when the cache updates', ( ) => {

						beforeEach( ( ) => {

							$httpBackend.flush();

						});

						it('should return the data from the cache', ( ) => {

							expect(cache.data()).toEqual(responseData);

							apiCacher.store(requestOptions, storedData);

							expect(cache.data()).toEqual(storedData);

						});

						it('should call the update listeners', ( ) => {

							let spy = jasmine.createSpy('update');

							cache.onUpdate(spy);

							spy.calls.reset();

							apiCacher.store(requestOptions, storedData);

							expect(spy).toHaveBeenCalled();

						});

					});
				});

				describe('with a 400', ( ) => {

					beforeEach( ( ) => {

						$httpBackend.expectGET(requestOptions.url)
							.respond(400, error);

						cache = resourceCache(requestOptions, cacheOptions);

					});

					it('should return rejected as state', ( ) => {

						expect(cache.state()).toBe('pending');

						$httpBackend.flush();

						expect(cache.state()).toBe('rejected');

						expect(cache.data()).toEqual(error);

					});

					it('should call the update listeners', ( ) => {

						let spy = jasmine.createSpy('update');

						cache.onUpdate(spy);

						spy.calls.reset();

						$httpBackend.flush();

						expect(spy).toHaveBeenCalled();

					});

				});

				describe('with a 0', ( ) => {

					beforeEach( ( ) => {

						$httpBackend.expectGET(requestOptions.url)
							.respond(0);

						cache = resourceCache(requestOptions, cacheOptions);

					});

					describe('with data', ( ) => {

						beforeEach( ( ) => {

							apiCacher.store(requestOptions, storedData);

						});

						it('should ignore the failed request', ( ) => {

							expect(cache.hasData()).toBe(true);

							expect(cache.state()).toBe('resolved');

							$httpBackend.flush();

							expect(cache.hasData()).toBe(true);

							expect(cache.state()).toBe('resolved');

						});

					});

					describe('without data', ( ) => {

						it('should transition to rejected', ( ) => {

							expect(cache.state()).toBe('pending');

							expect(cache.hasData()).toBe(false);

							$httpBackend.flush();

							expect(cache.hasData()).toBe(false);

							expect(cache.state()).toBe('rejected');

						});

					});

				});
			});

			describe('with cache', ( ) => {

				beforeEach( ( ) => {
					cacheOptions = { };

					apiCacher.store(requestOptions, storedData);
				});

				describe('with a successful request', ( ) => {

					beforeEach( ( ) => {

						$httpBackend.expectGET(requestOptions.url)
							.respond(responseData);

						cache = resourceCache(requestOptions, cacheOptions);

					});

					it('should return the data from the cache', ( ) => {

						expect(cache.data()).toEqual(storedData);

						$httpBackend.flush();

					});

					it('should be resolved', ( ) => {

						expect(cache.state()).toBe('resolved');

						$httpBackend.flush();

					});

					it('should return the data from the request once resolved', ( ) => {

						$httpBackend.flush();

						expect(cache.data()).toEqual(responseData);

					});

				});

				describe('with a failed request', ( ) => {

					beforeEach( ( ) => {

						$httpBackend.expectGET(requestOptions.url)
							.respond(400, error);

						cache = resourceCache(requestOptions, cacheOptions);

					});

					it('should transition to rejected after the request is completed', ( ) => {

						expect(cache.state()).toBe('resolved');

						$httpBackend.flush();

						expect(cache.state()).toBe('rejected');

					});

					it('should return the error as data', ( ) => {

						$httpBackend.flush();

						expect(cache.data()).toEqual(error);

					});

				});
				
			});

		});

		describe('with every defined', ( ) => {

			beforeEach( ( ) => {

				cacheOptions = { every: 30000 };

			});

			describe('if the cache is stale', ( ) => {

				beforeEach( ( ) => {

					let time = Date.now();

					spyOn(Date, 'now').and.returnValue(time - cacheOptions.every - 1);

					apiCacher.store(requestOptions, storedData);

					expect(Date.now).toHaveBeenCalled();

					Date.now.and.callThrough();

					$httpBackend.expectGET(requestOptions.url)
						.respond(storedData);

					cache = resourceCache(requestOptions, cacheOptions);

				});

				it('should fetch data', ( ) => {

					expect($httpBackend.flush).not.toThrow();

				});

			});

			describe('if the cache is not state', ( ) => {

				beforeEach( ( ) => {

					apiCacher.store(requestOptions, storedData);

					cache = resourceCache(requestOptions, cacheOptions);

				});

				it('should not fetch data', ( ) => {

					$httpBackend.verifyNoOutstandingRequest();

				});

			});

		});

		describe('when changing request options', ( ) => {

			let updatedData = { foo: 'foo' },
				newOpts;

			beforeEach( ( ) => {

				$httpBackend.expectGET(requestOptions.url)
					.respond(storedData);

				cache = resourceCache(requestOptions);

			});

			describe('to another url', ( ) => {

				beforeEach( ( ) => {
					newOpts = { url: '/bar' };
				});

				describe('before resolving', ( ) => {

					it('should cancel the running request and start another one', ( ) => {

						$httpBackend.expectGET(newOpts.url)
							.respond(updatedData);

						cache.setRequestOptions(newOpts);

						$httpBackend.flush(1);

						$httpBackend.verifyNoOutstandingRequest();
						$httpBackend.verifyNoOutstandingExpectation();

					});

					it('should not be rejected at any point', ( ) => {

						expect(cache.state()).toBe('pending');

						$httpBackend.expectGET(newOpts.url)
							.respond(updatedData);

						cache.setRequestOptions(newOpts);

						expect(cache.state()).toBe('pending');

						$httpBackend.flush();

						expect(cache.state()).toBe('resolved');

					});

				});

			});

			describe('to null', ( ) => {

				beforeEach( ( ) => {

					newOpts = null;

					$httpBackend.flush();

				});

				it('should be resolved', ( ) => {

					cache.setRequestOptions(newOpts);

					expect(cache.state()).toBe('resolved');

				});

				it('should return no data', ( ) => {

					cache.setRequestOptions(newOpts);

					expect(cache.hasData()).toBe(false);

					expect(cache.data()).toBe(null);

				});

				it('should not fetch anything', ( ) => {

					$httpBackend.verifyNoOutstandingRequest();

				});

			});

		});

		describe('when destroyed', ( ) => {

			describe('with a running request', ( ) => {

				beforeEach( ( ) => {

					$httpBackend.expectGET(requestOptions.url)
						.respond(responseData);

					cache = resourceCache(requestOptions);

				});

				it('should cancel running requests', ( ) => {
					
					cache.destroy();

					$httpBackend.verifyNoOutstandingRequest();

				});

			});

		});

	});

	describe('with cache disabled', ( ) => {

		beforeEach( ( ) => {

			cacheOptions = { disabled: true };

			$httpBackend.expectGET(requestOptions.url)
				.respond(responseData);

			cache = resourceCache(requestOptions, cacheOptions);

		});

		it('should ignore cached responses', ( ) => {

			apiCacher.store(requestOptions, storedData);

			expect(cache.data()).toBeNull();

			expect(cache.state()).toBe('pending');

			$httpBackend.flush();

			expect(cache.data()).toEqual(responseData);

		});

		it('should not store the response in the cache', ( ) => {

			$httpBackend.flush();

			expect(apiCacher.get(requestOptions)).toBeUndefined();

		});

	});

	describe('when converting to a promise', ( ) => {

		let promise,
			data = [ 'foo' ],
			err = 'err',
			resolved,
			rejected;

		let initCache = ( ) => {

			cache = resourceCache({ url: '/foo' } );

			resolved = jasmine.createSpy('resolved');
			rejected = jasmine.createSpy('rejected');

			promise = cache.asPromise();

			promise.then(resolved);
			promise.catch(rejected);
		};

		describe('without cache', ( ) => {

			describe('when resolved', ( ) => {

				beforeEach( ( ) => {

					$httpBackend.expectGET('/foo')
						.respond(data);

					initCache();

				});

				it('should return a promise', ( ) => {

					expect(promise).toBeDefined();

					expect('then' in promise).toBe(true);

					$httpBackend.flush();

				});

				it('should when the request completes', ( ) => {

					$httpBackend.flush();

					expect(resolved).toHaveBeenCalledWith(data);

				});

			});

			describe('when rejected', ( ) => {

				beforeEach( ( ) => {

					$httpBackend.expectGET('/foo')
						.respond(400, err);

					initCache();

				});

				it('should reject with the error', ( ) => {

					$httpBackend.flush();

					expect(rejected).toHaveBeenCalledWith(jasmine.objectContaining({ data: err }));

				});

			});

		});

		describe('with cache', ( ) => {

			beforeEach( ( ) => {

				apiCacher.store('/foo', data);

				$httpBackend.expectGET('/foo')
					.respond(data);

				initCache();

			});

			it('should resolve with the data from the cache', ( ) => {

				$rootScope.$digest();

				expect(resolved).toHaveBeenCalledWith(data);

				$httpBackend.flush();

			});

		});

	});

	describe('when invalidating resources', ( ) => {

		let url = '/foo',
			data = { foo: 'bar' };

		beforeEach( ( ) => {

			$httpBackend.expectGET(url)
				.respond(data);

			resourceCache({ url }, { disabled: true } );

			$httpBackend.flush();

		});

		describe('and clauses match', ( ) => {

			it('should reload the resource when a string is the same as a url', ( ) => {

				$httpBackend.expectGET(url)
					.respond(data);

				resourceCache.invalidate([ url ]);

				$httpBackend.flush(1);

				$httpBackend.verifyNoOutstandingExpectation();

			});

			it('should reload the resource when a regex matches the url', ( ) => {

				$httpBackend.expectGET(url)
					.respond(data);

				resourceCache.invalidate([ /.*?/ ]);

				$httpBackend.flush();

			});

		});

		describe('and clauses do not match', ( ) => {

			it('should not reload the resource', ( ) => {

				resourceCache.invalidate([ 'bar' ]);

				$httpBackend.verifyNoOutstandingRequest();

			});

		});

	});

	afterEach( ( ) => {
		apiCacher.clear();
	});

});
