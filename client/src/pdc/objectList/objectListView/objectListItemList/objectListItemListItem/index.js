import angular from 'angular';
import template from './template.html';

import './styles.scss';

export default
		angular.module('Zaaksysteem.pdc.objectListItemListItem', [
		])
		.directive('objectListItemListItem', [ ( ) => {
			return {
				restrict: 'E',
				template,
				scope: {
					objectItem: '&'
				}
			};
		}
		])
		.name;
