/*global define,fetch*/
(function ( ) {
	
	window.zsDefine('nl.mintlab.docs.Folder', function ( ) {
		
		var inherit = window.zsFetch('nl.mintlab.utils.object.inherit'),
			StoredEntity = window.zsFetch('nl.mintlab.docs.StoredEntity'),
			File = window.zsFetch('nl.mintlab.docs.File'),
			indexOf = window.zsFetch('nl.mintlab.utils.shims.indexOf');
		
		function Folder ( ) {
			this._folders = [];
			this._files = [];
			this._childrenByUid = {};
			this._entityType = 'folder';
			this._collapsed = false;
			Folder.uber.constructor.apply(this, arguments);
		}
		
		inherit(Folder, StoredEntity);
		
		Folder.prototype.add = function ( child ) {
			var collection,
				parent = child.getParent();
			if(parent === this) {
				return;
			}
			if(child instanceof Folder) {
				collection = this._folders;
			} else if(child instanceof File) {
				collection = this._files;
			}
			if(parent) {
				parent.remove(child);
			}
			collection.push(child);
			child.setDepth(this.getDepth() + 1);
			child.setParent(this);
			this._childrenByUid[child.getUid()] = child;
			this.publish('add', child);
		};
		
		Folder.prototype.remove = function ( child ) {
			var collection,
				parent = child.getParent();
			if(parent !== this) {
				throw new Error('Child ' + child.name + ' is not in this folder');
			}
			if(child instanceof Folder) {
				collection = this._folders;
			} else {
				collection = this._files;
			}
			
			var index = indexOf(collection, child);
			collection.splice(index, 1);
			child.setParent(null);
			child.setDepth(-1);
			delete this._childrenByUid[child.getUid()];
			this.publish('remove', child);
		};
		
		Folder.prototype.getFolders = function ( ) {
			return this._folders;
		};
		
		Folder.prototype.getFiles = function ( ) {
			return this._files;
		};
		
		Folder.prototype.getChildByUid = function ( uid ) {
			return this._childrenByUid[uid];
		};
		
		Folder.prototype.getEntityByPath = function ( path ) {
			var child = this.getChildByUid(path[0]);
			if(!child) {
				return null;
			}
			var subPath = path.concat();
			subPath.shift();
			if(subPath.length === 0) {
				return child;
			} else if(child instanceof Folder) {
				return child.getEntityByPath(subPath);
			}
			return null;
		};
		
		Folder.prototype.empty = function ( ) {
			
			while(this._folders.length) {
				this.remove(this._folders[0]);
			}
			
			while(this._files.length) {
				this.remove(this._files[0]);
			}
			
		};
		
		Folder.prototype.isCollapsed = function ( ) {
			return this._collapsed;
		};
		
		Folder.prototype.setCollapsed = function ( c ) {
			this._collapsed = c;
		};
		
		return Folder;
	});
	
})();
