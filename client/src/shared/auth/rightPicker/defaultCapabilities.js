export default ( ) => {
	return [
		{
			name: 'manage',
			label: 'Beheren'
		},
		{
			name: 'write',
			label: 'Behandelen'
		},
		{
			name: 'read',
			label: 'Raadplegen'
		},
		{
			name: 'search',
			label: 'Zoeken'
		}
	].reverse();
};
