import openAction from './../openAction';
import inputSelectFirst from './../../../../common/input/inputSelectFirst';

export default ( casetype ) => {

	let form = $('zs-case-admin-view form');

	openAction('Zaaktype wijzigen');

	inputSelectFirst(form, 'casetype', casetype);

	form.submit();

};
