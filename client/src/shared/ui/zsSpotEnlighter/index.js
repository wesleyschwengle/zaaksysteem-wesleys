import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import auxiliaryRouteModule from './../../util/route/auxiliaryRoute';
import savedSearchesServiceModule from './savedSearchesService';
import zsSpotEnlighterSuggestionListModule from './zsSpotEnlighterSuggestionList';
import objectSuggestionServiceModule from './../../object/zsObjectSuggest/objectSuggestionService';
import composedReducerModule from './../../api/resource/composedReducer';
import resourceModule from './../../api/resource';
import contextualActionServiceModule from './../zsContextualActionMenu/contextualActionService';
import zsDropdownMenuModule from './../zsDropdownMenu';
import mutationServiceModule from './../../api/resource/mutationService';
import actionsModule from './../../../intern/zsIntern/zsActiveSubject/actions';
import immutable from 'seamless-immutable';
import zsSpinnerModule from './../zsSpinner';
import get from 'lodash/get';
import assign from 'lodash/assign';
import find from 'lodash/find';
import capitalize from 'lodash/capitalize';
import isArray from 'lodash/isArray';
import flatten from 'lodash/flatten';
import includes from 'lodash/includes';
import debounce from 'lodash/debounce';
import fuzzy from 'fuzzy';
import links from './../../navigation/zsSideMenu/links';
import zsTooltipModule from './../zsTooltip';
import template from './template.html';
import './spotenlighter.scss';

export default
	angular.module('zsUniversalSearch', [
			angularUiRouterModule,
			auxiliaryRouteModule,
			savedSearchesServiceModule,
			objectSuggestionServiceModule,
			zsSpotEnlighterSuggestionListModule,
			zsSpinnerModule,
			composedReducerModule,
			resourceModule,
			zsDropdownMenuModule,
			mutationServiceModule,
			zsTooltipModule,
			actionsModule,
			contextualActionServiceModule
		])
		// we use zsUniversalSearch to prevent naming clashes w/ legacy zsSpotEnlighter
		.directive('zsUniversalSearch', [
			'$window', '$timeout', '$sce', '$location', '$document', 'resource', 'composedReducer', 'savedSearchesService', 'objectSuggestionService', 'contextualActionService', '$state', 'auxiliaryRouteService', 'mutationService',
			($window, $timeout, $sce, $location, $document, resource, composedReducer, savedSearchesService, objectSuggestionService, contextualActionService, $state, auxiliaryRouteService, mutationService ) => {

			const CACHE_INVALIDATION_TIME = 5 * 60 * 1000;

			let iconMappings =
				{
					case: 'folder-outline',
					saved_search: 'file-find',
					file: 'file',
					natuurlijk_persoon: 'account',
					bedrijf: 'domain',
					vraag: 'help',
					product: 'package-variant-closed',
					defaults: 'hexagon-outline'
				},
				getIcon = ( type ) => iconMappings[type] || iconMappings.defaults;

			return {
				restrict: 'E',
				template,
				scope: {
					isActive: '&',
					useLocation: '&',
					user: '&',
					onOpen: '&',
					onClose: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( $scope, $element ) {

					let ctrl = this,
						input = $element.find('input'),
						savedSearchesListResource,
						savedSearchesMatchingResource,
						savedSearchesReducer,
						suggestionResource,
						linkReducer,
						objectTypeResource,
						objectTypeReducer,
						suggestionReducer,
						changeHandlerReducer,
						open = false,
						selectedObjectType = 'all',
						committedQuery = '';

					let getActions = ( item ) => {

						let actions = [];

						switch (item.type) {
							case 'natuurlijk_persoon':
							case 'bedrijf':
							actions = actions.concat({
								name: 'activate',
								label: 'Activeer',
								click: ( event ) => {

									mutationService.add({
										type: 'active_subject/enable',
										// make sure mutation gets picked up by resource
										request: {
											url: '/betrokkene/get_session',
											params: {
												zapi_num_rows: 20,
												zapi_page: 1
											}
										},
										data: {
											subjectId: `betrokkene-${item.data.object_type}-${item.data.id}`,
											name: item.label
										}
									}).asPromise()
										.then(( ) => {
											ctrl.close();
										});

									event.preventDefault();
									event.stopPropagation();
								}
							});
							break;
						}

						return actions;
					};

					let commitQueryChange = ( ) => {
						committedQuery = ctrl.query;
					};

					savedSearchesListResource = resource(savedSearchesService.getRequestOptions(), { scope: $scope });

					savedSearchesMatchingResource = resource(
						( ) => {
							let opts;

							if (open && committedQuery && (selectedObjectType === 'all' || selectedObjectType === 'saved_search')) {
								opts = savedSearchesService.getRequestOptions(committedQuery);
							} else {
								opts = null;
							}
							return opts;
						},
						{ scope: $scope, cache: { disabled: true } }
					);

					suggestionResource = resource(
						( ) => {

							let opts = null;

							if (open && (selectedObjectType !== 'saved_search' && selectedObjectType !== 'shortcut')) {
								opts = objectSuggestionService.getRequestOptions(
									committedQuery,
									selectedObjectType === 'all' ? '' : selectedObjectType
								);
							}

							return opts;
						},
						{ scope: $scope, cache: { disabled: true } }
					)
						.reduce(( requestOptions, data ) => {

							let suggestions = data;
							
							if (!isArray(suggestions)) {
								suggestions = immutable([]);
							}

							suggestions = objectSuggestionService.reduce(suggestions || immutable([]))
								.map( ( suggestion ) => {
									return suggestion.merge({ iconClass: getIcon(suggestion.type), actions: getActions(suggestion) });
								});

							return suggestions;
						});

					linkReducer = composedReducer( { scope: $scope }, ( ) => selectedObjectType, ctrl.user)
						.reduce(( objectType, user ) => {

							return objectType === 'shortcut' ?
								flatten(links({ user, auxiliaryRouteService, $state }).map(cat => cat.children))
								: null;
						});

					objectTypeResource = resource(objectSuggestionService.getRequestOptions('', 'objecttypes'), { scope: $scope, cache: { every: CACHE_INVALIDATION_TIME } })
						.reduce( ( requestOptions, data ) => {
							return immutable(
								[
									{
										name: 'all',
										label: 'Zoek in alles'
									},
									{
										name: 'saved_search',
										label: 'Zoekopdrachten'
									},
									{
										name: 'contact',
										label: 'Contacten'
									},
									{
										name: 'file',
										label: 'Documenten'
									},
									{
										name: 'shortcut',
										label: 'Snelkoppelingen'
									}
								]
							)
								.concat(
									(data || [])
										.map( ( type ) => {
											return {
												name: type.object_type,
												label: type.label
											};
										})
								);
						});

					objectTypeReducer = composedReducer({ scope: $scope, waitUntilResolved: false }, objectTypeResource, ( ) => selectedObjectType)
						.reduce(( objectTypes, selected ) => {

							return objectTypes.map( ( item ) => {
								return item.merge({
									iconClass: item.name === selected ? 'checkbox-marked-circle' : 'checkbox-blank-circle-outline',
									click: ( ) => {
										ctrl.selectObjectType(item.name);
										$timeout(( ) => {
											$element.find('input')[0].focus();
										}, false);
									}
								});

							});
						});

					savedSearchesReducer = composedReducer({ waitUntilResolved: false, scope: $scope }, savedSearchesListResource, savedSearchesMatchingResource, ( ) => selectedObjectType)
						.reduce( ( listedSearches, matchedSearches, objectType ) => {
							let searches = immutable([]);
							
							if (objectType === 'all' || objectType === 'saved_search') {
								searches =
									(committedQuery ?
										matchedSearches

										: savedSearchesService.getPredefinedSearches().concat(listedSearches || [])
									)
									|| immutable([]);
							}

							return searches.map( ( item ) => {
								return {
									id: item.id,
									type: 'saved_search',
									label: item.label,
									iconClass: getIcon('saved_search'),
									link: `/search/${item.id}`,
									data: item.data
								};
							});

						});

					suggestionReducer = composedReducer({ scope: $scope }, suggestionResource, savedSearchesReducer, ( ) => open, ( ) => committedQuery, ( ) => selectedObjectType, linkReducer)
						.reduce( ( loadedSuggestions, savedSearches, isOpen, query, objectType, availableLinks ) => {

							let matches,
								suggestions = loadedSuggestions || [];

							if (!isOpen) {
								return [];
							}

							suggestions =
								suggestions.concat(savedSearches)
									.asMutable({ deep: true });

							if (objectType === 'shortcut') {

								suggestions =
									suggestions.concat(
										contextualActionService.getAvailableActions()
											.filter(
												action => !query || includes(action.label.toLowerCase(), query.toLowerCase())
											)
											.map(action => {
												return {
													id: action.name,
													type: 'action',
													label: action.label,
													iconClass: action.iconClass,
													click: ( ) => {
														contextualActionService.openAction(action);
													}
												};
											})
									)
									.concat(
										availableLinks.filter(
												link => {
													return (link.when || link.when === undefined)
														&& (!query || includes(link.label.toLowerCase(), query.toLowerCase()));
												}
											)
											.map(link => {
												return {
													id: link.name,
													label: link.label,
													link: link.href,
													iconClass: link.icon
												};
											})
									);
							}

							matches =
								query ?
									fuzzy.filter(query || '', suggestions, {
										pre: '<strong>',
										post: '</strong>',
										extract: ( el ) => el.label
									})
										.map(el => assign(el.original, { label: el.string }))
									: suggestions;

							suggestions =
								matches
									.concat(
										suggestions.filter( suggestion => !find(matches, { id: suggestion.id }))
									)
									.map(suggestion => {
										return suggestion.type === 'case' ?
											assign(
												{},
												suggestion,
												{
													label: `${suggestion.data.zs_object.values['case.number']}: ${suggestion.data.zs_object.values['case.casetype.name']} <span class="case-status item-label item-label-default">${capitalize(get(suggestion.data, 'status_formatted', ''))}</span>`,
													description:
														'status_formatted' in suggestion.data ?
															`<span class="case-requestor" ${!suggestion.data.aanvrager ? ' hidden' : ''}><i class="mdi mdi-account"></i> ${get(suggestion.data, 'aanvrager.naam', '')}</span>
															<span class="case-extra-information" ${!suggestion.data.description ? ' hidden' : ''}><i class="mdi mdi-file-document-box"></i> ${get(suggestion.data, 'description') || ''}</span>`
															: ''
												}
											)
											: suggestion;
									})
									.map(suggestion => assign({}, suggestion, { label: $sce.trustAsHtml(suggestion.label), description: $sce.trustAsHtml(suggestion.description) }));

							return suggestions;
						});

					changeHandlerReducer = composedReducer( { scope: $scope }, ( ) => selectedObjectType)
						.reduce(objectType => {

							let fn =
								objectType === 'shortcut' ?
									commitQueryChange
									: debounce(
										( ) => {
											$scope.$evalAsync(commitQueryChange);
										},
										250,
										{ leading: false, trailing: true }
									);

							return fn;
						});

					ctrl.isEmpty = ( ) => suggestionReducer.state() === 'resolved' && get(suggestionReducer.data(), 'length', 0) === 0;

					ctrl.getSuggestions = ( ) => {
						return suggestionReducer.state() !== 'rejected' ? suggestionReducer.data() : null;
					};

					ctrl.getKeyInputDelegate = ( ) => ({ input });

					ctrl.open = ( ) => {
						open = true;
						ctrl.onOpen();
					};

					ctrl.close = ( ) => {
						open = false;
						selectedObjectType = 'all';
						ctrl.query = committedQuery = '';
						ctrl.onClose();
					};

					ctrl.handleBackClick = ctrl.close;

					ctrl.handleQueryChange = ( ) => changeHandlerReducer.data()();

					ctrl.handleMagnifyClick = ( ) => {
						ctrl.open();
						$timeout( ( ) => {
							input[0].focus();
						}, 0, false);
					};

					ctrl.isOpen = ( ) => open;

					ctrl.isLoading = ( ) => suggestionResource.request() && suggestionReducer.state() === 'pending';

					ctrl.getSuggestionListLabel = ( ) => {
						let isEmpty = ctrl.isEmpty(),
							hasErrored = suggestionReducer.state() === 'rejected',
							label = '';

						if (isEmpty && (committedQuery.length > 3 || selectedObjectType === 'shortcut')) {
							label = 'Geen resultaten gevonden voor deze zoekopdracht';
						} else if (hasErrored) {
							label = 'Er ging iets mis met het opvragen van de resultaten voor deze zoekopdracht.';
						} else if ( selectedObjectType !== 'shortcut' && (committedQuery.length > 0 && committedQuery.length < 3)) {
							label = 'Typ minimaal drie karakters om resultaten te tonen';
						}

						return label;
					};

					ctrl.handleSuggestionSelect = ( suggestion, event ) => {
						
						if (event instanceof KeyboardEvent && suggestion.link) {

							let base = $document.find('base').attr('href');

							if (ctrl.useLocation() && suggestion.link.indexOf(base) !== -1) {

								$location.url(suggestion.link.replace(base, '/'));

							} else {
								$window.location.href = suggestion.link;
							}
						} else if (typeof suggestion.click === 'function') {
							suggestion.click(event);
							event.preventDefault();
						}

						ctrl.close();
					};

					if (ctrl.isActive) {
						ctrl.isActive({ $getter: ctrl.isOpen });
					}

					ctrl.getObjectTypeOptions = objectTypeReducer.data;

					ctrl.query = committedQuery;

					ctrl.selectObjectType = ( type ) => {
						selectedObjectType = type;
					};

					ctrl.getObjectTypeLabel = ( ) => {
						let objectTypes = objectTypeReducer.data(),
							selectedItem;

						selectedItem =
							objectTypes.filter(item => item.name === selectedObjectType)[0];

						return selectedItem.label;
					};

					ctrl.getSelectedObjectType = ( ) => selectedObjectType;

					ctrl.handleObjectTypeChange = ( objectType ) => {
						selectedObjectType = objectType;
					};

					$element.find('input').bind('focus', ( event ) => {

						if (!open && !$element[0].contains(event.relatedTarget)) {
							$scope.$apply(ctrl.open);
						}
						
					});

					$document.bind('keyup', ( event ) => {
						if (event.keyCode === 27) {

							input[0].blur();

							$scope.$evalAsync(( ) => {
								ctrl.close();
							});

						} else if (event.keyCode === 191 && event.ctrlKey) {

							if (event.shiftKey) {
								selectedObjectType = 'shortcut';
							}

							input[0].focus();

						}
					});

				}],
				controllerAs: 'vm'
			};
		}])
		.name;
