BEGIN;

    CREATE TABLE IF NOT EXISTS alternative_authentication_activation_link (
        token TEXT PRIMARY KEY NOT NULL,
        subject_id UUID NOT NULL REFERENCES subject(uuid),
        expires TIMESTAMP WITHOUT TIME ZONE NOT NULL
    );

    CREATE TABLE IF NOT EXISTS gegevensmagazijn_subjecten (
        subject_uuid UUID UNIQUE NOT NULL REFERENCES subject(uuid),
        np_uuid UUID REFERENCES natuurlijk_persoon(uuid),
        nnp_uuid UUID REFERENCES bedrijf(uuid)
    );

    ALTER TABLE gegevensmagazijn_subjecten ADD CONSTRAINT gegevensmagazijn_subjecten_xor_uuid CHECK (
        np_uuid IS NULL != nnp_uuid IS NULL
    );


COMMIT;
