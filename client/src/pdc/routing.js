import angular from 'angular';
import uiRouter from 'angular-ui-router';
import objectList from './objectList';
import objectDetail from './objectDetail';
import snackbarServiceModule from './../shared/ui/zsSnackbar/snackbarService';
import pdcAppModule from './shared/pdcApp';
import zsUiViewProgressModule from './../shared/ui/zsNProgress/zsUiViewProgress';
import merge from 'lodash/merge';
import flatten from 'lodash/flatten';
import first from 'lodash/first';
import get from 'lodash/get';
import createManifest from './shared/manifest';

export default angular.module('Zaaksysteem.pdc.routing', [
	uiRouter,
	objectList.moduleName,
	objectDetail.moduleName,
	snackbarServiceModule,
	pdcAppModule,
	zsUiViewProgressModule
])
	.config([ '$stateProvider', '$urlRouterProvider', '$locationProvider', ( $stateProvider, $urlRouterProvider, $locationProvider ) => {

		$locationProvider.html5Mode(true);

		$urlRouterProvider.otherwise(( $injector ) => {

			let $state = $injector.get('$state');

			$state.go('objectList');

		});

		$stateProvider.state({
			name: 'root',
			template: '<pdc-app app-config="config.data()"></pdc-app>',
			resolve: {
				config: [ '$rootScope', '$stateParams', '$q', 'resource', 'snackbarService', ( $rootScope, $stateParams, $q, resource, snackbarService ) => {

					let configResource = resource('/api/v1/app/app_pdc', {
						scope: $rootScope,
						cache: {
							every: 60 * 1000
						}
					})
						.reduce( ( requestOptions, data ) => {
							return first(data) || { instance: { interface_config: {} } };
						});

					return configResource.asPromise()
						.then(( ) => configResource)
						.catch( err => {
							snackbarService.error('Er ging iets fout bij het ophalen van de configuratie. Neem contact op met uw beheerder voor meer informatie.');
							return $q.reject(err);
						});

				}]
			},
			controller: [ '$scope', 'config', '$document', ( $scope, config, $document ) => {

				$scope.config = config;

				let styleEl;

				config.onUpdate( data => {

					createManifest( get(data, 'instance.interface_config'), $document );

					let color = get(data, 'instance.interface_config.accent_color', '#FFF'),
						rules = [
							`#nprogress .bar {
							  background: ${color};
							  height: 100%;
							}`,
							`#nprogress .peg {
							  box-shadow: 0 0 20px ${color}, 0 0 10px #FFF;
							}`,
							`#nprogress .spinner-icon {
							  border-top-color: ${color};
							  border-left-color: ${color};
							}`,
							`object-list-item-list-item .object-list-item--title{
							  color: ${color} !important;
							}`,
							`object-list-item-list-item > a:hover{
								color: ${color} !important;
							}`,
							`.object-list-view__supportlink a{
								color: ${color} !important;
							}`
						];

					if (styleEl) {
						styleEl.remove();
					}

					styleEl = $document[0].createElement('style');

					$document[0].head.appendChild(styleEl);

					rules.forEach(( rule, index ) => {
						styleEl.sheet.insertRule(rule, index);
					});

				});

			}]
		});

		flatten(
			[objectList, objectDetail]
				.map(routeConfig => routeConfig.config)
			)
			.forEach(route => {

				let mergedState =
					merge(route.route, {
						parent: 'root'
					});

				$stateProvider.state(route.state, mergedState);

			});

	}])
	.run([ '$rootScope', ( $rootScope ) => {

		$rootScope.$on('$stateChangeError', ( ...rest ) => {
			console.log(...rest);
		});

	}])
	.name;
