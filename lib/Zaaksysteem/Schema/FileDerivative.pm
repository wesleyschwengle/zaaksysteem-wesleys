package Zaaksysteem::Schema::FileDerivative;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::FileDerivative

=cut

__PACKAGE__->table("file_derivative");

=head1 ACCESSORS

=head2 id

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 file_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 filestore_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 max_width

  data_type: 'integer'
  is_nullable: 0

=head2 max_height

  data_type: 'integer'
  is_nullable: 0

=head2 date_generated

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 0
  original: {default_value => \"now()"}

=head2 type

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "file_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "filestore_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "max_width",
  { data_type => "integer", is_nullable => 0 },
  "max_height",
  { data_type => "integer", is_nullable => 0 },
  "date_generated",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
    original      => { default_value => \"now()" },
  },
  "type",
  { data_type => "text", is_nullable => 0 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 file_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::File>

=cut

__PACKAGE__->belongs_to("file_id", "Zaaksysteem::Schema::File", { id => "file_id" });

=head2 filestore_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Filestore>

=cut

__PACKAGE__->belongs_to(
  "filestore_id",
  "Zaaksysteem::Schema::Filestore",
  { id => "filestore_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2016-06-13 11:37:13
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:kxB3DhSBTUgE5PZG3tuLJg


1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
