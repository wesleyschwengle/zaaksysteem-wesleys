import login from './../../functions/common/auth/login';
import logout from './../../functions/common/auth/logout';
import loginAs from './../../functions/common/auth/loginAs';

describe('when opening a case and adding a note', ( ) => {

	beforeAll(( ) => {

		logout();

		loginAs('burgemeester');

		browser.get('/vergadering/bbv');

		let firstProposal = element.all(by.css('.proposal-item-table tbody tr')).first(),
			firstVote = element.all(by.css('.vote-button')).first(),
			voteTextarea = $('textarea'),
			voteSave = $('.proposal-vote-view__header-buttons button:nth-child(2)');

		firstProposal.click();

		browser.waitForAngular();

		firstVote.click();

		voteTextarea.sendKeys('this is my comment');

		voteSave.click();

	});

	it('the note should have the given content', ( ) => {

		let voteResult = $('.proposal-detail-view__list li:nth-child(7) .proposal-detail-view__value'),
			voteComment = $('.proposal-detail-view__list li:nth-child(12) .proposal-detail-view__value');

		expect(voteResult.getText()).toEqual('Akkoord');
		expect(voteComment.getText()).toEqual('this is my comment');

	});

	afterAll(( ) => {

		logout();

		login();

		browser.get('/intern');

	});

});
