package Zaaksysteem::Schema::GegevensmagazijnSubjecten;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::GegevensmagazijnSubjecten

=cut

__PACKAGE__->table("gegevensmagazijn_subjecten");

=head1 ACCESSORS

=head2 subject_uuid

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 0
  size: 16

=head2 np_uuid

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 1
  size: 16

=head2 nnp_uuid

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 1
  size: 16

=cut

__PACKAGE__->add_columns(
  "subject_uuid",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 0, size => 16 },
  "np_uuid",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 1, size => 16 },
  "nnp_uuid",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 1, size => 16 },
);
__PACKAGE__->add_unique_constraint(
  "gegevensmagazijn_subjecten_subject_uuid_key",
  ["subject_uuid"],
);

=head1 RELATIONS

=head2 np_uuid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::NatuurlijkPersoon>

=cut

__PACKAGE__->belongs_to(
  "np_uuid",
  "Zaaksysteem::Schema::NatuurlijkPersoon",
  { uuid => "np_uuid" },
);

=head2 subject_uuid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Subject>

=cut

__PACKAGE__->belongs_to(
  "subject_uuid",
  "Zaaksysteem::Schema::Subject",
  { uuid => "subject_uuid" },
);

=head2 nnp_uuid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Bedrijf>

=cut

__PACKAGE__->belongs_to(
  "nnp_uuid",
  "Zaaksysteem::Schema::Bedrijf",
  { uuid => "nnp_uuid" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2017-01-24 14:12:20
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:SKUr1sVCsGuDvnihPBxZtw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__


=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
