/*global angular,_,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.core.detail')
		.controller('nl.mintlab.core.detail.DetailFieldController', [ '$scope', '$parse', 'translationService', function ( $scope, $parse, translationService ) {
			
			var safeApply = window.zsFetch('nl.mintlab.utils.safeApply');
			
			$scope.editMode = false;
			
			$scope.setEditMode = function ( editMode ) {
				$scope.editMode = editMode;
			};
						
			$scope.getFieldValue = function ( ) {
				var resolve = $scope.field.resolve + ($scope.field.filter ? '|' + $scope.field.filter : ''),
					getter = $parse(resolve),
					val = getter($scope.item);
					
				if(val === undefined || val === null) {
					val = '';
				}
				return val;
			};
			
			$scope.getFieldTemplate = function ( ) {
				return $scope.field.template ? $scope.field.template : $scope.getFieldValue();
			};
			
			$scope.getForm = function ( ) {
				if(!$scope.field.form || !$scope.editMode) {
					return null;
				}
				
				var config,
					name = $scope.field.form.name || $scope.field.name,
					value = $scope.field.form.value || $scope.item[name],
					field = _.assign($scope.field.form, {
						name: name,
						value: value,
						'default': value
					});
				
				config = {
					"name": $scope.field.name,
					"actions": [
						{
							"type": "submit",
							"data": {
								"url": $scope.url + '/update'
							}
						}
					],
					"fieldsets": [
						{
							"fields": [ field ]
						}
					]
				};
				
				return config;
			};
			
			$scope.getFormValue = function ( ) {
				var name = $scope.field.form.name || $scope.field.name;
				return $scope.item[name];	
			};
			
			$scope.isEditable = function ( ) {
				return $scope.field.form;	
			};
			
			$scope.$on('form.change', function ( /*event, field*/ ) {
				safeApply($scope, function ( ) {
					// $scope.save();
					$scope.editMode = false;
				});
			});
			
			$scope.$on('form.submit.success', function ( event, name, data ) {
				var item = $scope.item,
					itemData = data.result[0];
				
				for(var key in itemData) {
					item[key] = itemData[key];
				}
			});
			
			$scope.$on('form.submit.error', function ( /*event, name, data*/ ) {
				$scope.$emit('systemMessage', {
					type: 'error',
					content: translationService.get('Waarde kon niet worden gewijzigd')
				});
			});
			
		}]);
	
})();
