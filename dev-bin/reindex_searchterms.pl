#!/usr/bin/perl

use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Getopt::Long;
use Zaaksysteem::Config;
use Pod::Usage;
use File::Spec::Functions;
use File::Basename;

use Zaaksysteem::Constants qw/OBJECTSEARCH_TABLENAMES/;


use Time::HiRes qw(gettimeofday tv_interval);

my %opt = (
    help       => 0,
    config     => '/etc/zaaksysteem/zaaksysteem.conf',
    n          => 0,
);

{
    local $SIG{__WARN__};
    my $ok = eval {
        GetOptions(
            \%opt, qw(
                help
                n
                config=s
                customer_d=s
                hostname=s
                tables=s@
                )
        );
    };
    if (!$ok) {
        pod2usage(1) ;
    }
}

pod2usage(0) if ($opt{help});

foreach (qw(config hostname)) {
    if (!defined $opt{$_}) {
        warn "Missing option: $_";
        pod2usage(1) ;
    }
}

if (!defined $opt{customer_d}) {
    $opt{customer_d} = catdir(dirname($opt{config}), 'customer.d');
}

my $ZS = Zaaksysteem::Config->new(
    zs_customer_d => $opt{customer_d},
    zs_conf       => $opt{config},
);

my $schema = $ZS->get_customer_schema($opt{hostname}, 1);

$opt{tables} = (($opt{tables} && !ref($opt{tables})) ? [ $opt{tables} ] : $opt{tables});

reindex_searchterms($schema);

sub reindex_searchterms {
    my $dbic        = shift;

    my $tablenames  = OBJECTSEARCH_TABLENAMES;

    foreach my $tablename (keys %$tablenames) {
        if($opt{tables} && @{ $opt{tables} }) {
            next unless grep {$_ eq $tablename } @{ $opt{tables} };
        }

        warn ("Reindexing table: $tablename\n");

        do_transaction(
            $dbic,
            sub {
                reindex_table($dbic, $tablenames->{$tablename}->{tablename});        
            }
        );
    }
}

sub reindex_table {
    my ($dbic, $tablename) = @_;

    my $resultset = $dbic->resultset($tablename)->search();
    my $totalcount = $resultset->count;
    my $count = $totalcount;

    warn("Updating number of records: $totalcount\n");
    while(my $row = $resultset->next()) {
        if (--$count % 25 == 0) {
            warn ("Records left: $count of $totalcount\n");
        }
        # update handles reindex, see ComponentZaak:update()
        $row->update();
    }
}

sub do_transaction {
    my ($dbic, $sub) = @_;

    $dbic->txn_do(
        sub {
            $sub->();
            if ($opt{n}) {
                $dbic->txn_rollback;
            }
        }
    );
}

1;

__END__

=head1 NAME

reindex_searchterms.pl - Reindex a table by calling update on every record.

=head1 SYNOPSIS

reindex_searchterms.pl OPTIONS [ [ --tables NatuurlijkPersoon ] [--tables Bedrijf ] ]

=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * customer_d

The customer_d dir, defaults to the relative directory where zaaksysteem.conf is found.

=item * hostname

The hostname you want to touch cases for

=item * tables

You can use this to reindex specific tables, not supplying this will reindex all tables

=item * n

Dry run, run it, but don't

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
