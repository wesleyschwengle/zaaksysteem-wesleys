package Zaaksysteem::DB::ResultSet::ZaaktypeResultaten;

use Moose;

use DateTime;

extends qw[
    DBIx::Class::ResultSet
    Zaaksysteem::Zaaktypen::BaseResultSet
];

use constant PROFILE => {
    required        => [qw/
    /],
    optional        => [qw/
    /],
};

around _retrieve_as_session => sub {
    my $orig = shift;
    my $self = shift;

    my $rv = $self->$orig(@_);

    for my $row (values %{ $rv }) {
        $row->{ $_ } = $row->{ $_ } ? $row->{ $_ }->dmy : undef for qw[
            selectielijst_brondatum
            selectielijst_einddatum
        ]
    }

    return $rv;
};

sub _validate_session {
    my $self            = shift;
    my $profile         = PROFILE;
    my $rv              = {};

    $self->__validate_session(
        @_,
        $profile,
    );
}

sub _commit_session {
    my ($self, $node, $element_session_data) = @_;

    my $profile         = PROFILE;
    my $rv              = {};

    while (my ($key, $data) = each %{ $element_session_data }) {
        $data->{trigger_archival} //= 0;

        $data->{ $_ } = $self->_mangle_date($data->{ $_ }) for qw[
            selectielijst_brondatum
            selectielijst_einddatum
        ];
    }

    $self->next::method(
        $node,
        $element_session_data,
        {
            status_id_column_name   => 'zaaktype_status_id',
        }
    );
}

sub _mangle_date {
    my $self = shift;
    my $date = shift;

    return undef unless defined $date;
    return $date if blessed $date && $date->isa('DateTime');

    my ($day, $month, $year) = $date =~ m[^(\d{2})\-(\d{2})\-(\d{4})$];

    return undef unless $day && $month && $year;

    return DateTime->new(
        day => $day,
        month => $month,
        year => $year
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
