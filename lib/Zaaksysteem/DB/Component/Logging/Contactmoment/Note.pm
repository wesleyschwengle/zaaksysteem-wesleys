package Zaaksysteem::DB::Component::Logging::Contactmoment::Note;
use Moose::Role;

=head2 onderwerp

=cut

sub onderwerp {
    sprintf('Contactmoment toegevoegd');
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
