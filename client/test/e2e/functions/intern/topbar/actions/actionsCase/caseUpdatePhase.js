import openAction from './../openAction';

export default ( phase ) => {

	let form = $('zs-case-admin-view form');

	openAction('Fase wijzigen');

	$('[data-name="phase"] select').sendKeys(phase);

	form.submit();

};
