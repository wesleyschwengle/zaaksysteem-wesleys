import angular from 'angular';
import template from './template.html';
import resourceModule from '../../../shared/api/resource';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import angularUiRouterModule from 'angular-ui-router';
import appServiceModule from './../../shared/appService';
import every from 'lodash/every';
import seamlessImmutable from 'seamless-immutable';
import includes from 'lodash/includes';
import nGramFilter from '../../shared/nGramFilter';
import getCaseValueFromAttribute from '../../shared/getCaseValueFromAttribute';
import './styles.scss';

export default
		angular.module('Zaaksysteem.meeting.proposalsListView', [
			resourceModule,
			composedReducerModule,
			appServiceModule,
			angularUiRouterModule
		])
		.directive('proposalsListView', [ '$state', '$stateParams', '$rootScope', '$window', 'resource', 'composedReducer', 'appService', ( $state, $stateParams, $rootScope, $window, resource, composedReducer, appService ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					proposalsResource: '&',
					appConfig: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						filteredProposalReducer,
						proposalsReducer,
						itemsCollapsedState = {},
						noteProposalReducer,
						totalProposalsReducer,
						noteStorage = seamlessImmutable( JSON.parse( $window.localStorage.getItem('meetingAppNotes') ) );

					// Builds the filter suggestion index
					ctrl.proposalsResource().onUpdate(( data ) => {
						nGramFilter.addToIndex( data || [] );
					});

					filteredProposalReducer = composedReducer({ scope: $scope }, ctrl.proposalsResource(), ( ) => appService.state().filters )
						.reduce( ( proposals, filters ) => {

							let attributesToIndex = nGramFilter.getAttributesToIndex();

							return filters.length ?
								proposals.filter(
									proposal => {
										return filters.every( filter => {
											return attributesToIndex.map( attr => getCaseValueFromAttribute(attr, proposal))
												.some(val => val.indexOf(filter) !== -1);
										});
									}
								)
								: proposals;
						});

					totalProposalsReducer = composedReducer({ scope: $scope }, ctrl.proposalsResource, ( ) => $stateParams )
						.reduce( ( proposals, stateParams ) => {
							if ( proposals.totalRows() <= stateParams.paging ) {
								return false;
							}
							return true;
						});

					ctrl.isMoreAvailable = totalProposalsReducer.data;

					let getStorage = ( ) => {
						noteStorage = seamlessImmutable( JSON.parse( $window.localStorage.getItem('meetingAppNotes') ) );
					};

					if (!noteStorage) {
						$window.localStorage.setItem('meetingAppNotes', JSON.stringify([]));
						getStorage();
					}

					appService.on('proposal_note_save', ( ) => {
						getStorage();
					});

					noteProposalReducer = composedReducer({ scope: $scope }, filteredProposalReducer, ( ) => noteStorage)
						.reduce( ( proposals, notes ) => {

							let noteIds = notes.map( ( note ) => note.id);

							return proposals.map( ( proposal ) => {

								if ( includes( noteIds, proposal.instance.number ) ) {

									return proposal.merge( { notes: true } );
								}

								return proposal;
							});

						});

					proposalsReducer = composedReducer({ scope: $scope }, noteProposalReducer)
						.reduce( filteredProposals => {
							return [
									{
										id: null,
										label: 'Alle voorstellen',
										children: filteredProposals
									}
								];

						});

					ctrl.getProposals = proposalsReducer.data;

					ctrl.handleToggle = ( id ) => {

						let isStateExpanded = appService.state().expanded,
							allCollapsed;

						itemsCollapsedState[id] = ctrl.isExpanded(id);

						allCollapsed = every(proposalsReducer.data(), ( meeting ) => !ctrl.isExpanded(meeting.id));

						if (isStateExpanded && allCollapsed) {
							appService.dispatch('toggle_expand');
						}
					};

					ctrl.isExpanded = ( id ) => {
						return itemsCollapsedState.hasOwnProperty(id) ? !itemsCollapsedState[id] : !!appService.state().expanded;
					};

                    ctrl.isGrouped = ( ) => !!($state.current.name === 'meetingList');

					ctrl.isLoading = ( ) => proposalsReducer.state() === 'pending';

					ctrl.loadMore = ( ) => {
						$state.go($state.current.name, { paging: $stateParams.paging + 50 });
					};

					$scope.$on(
						'$destroy',
						appService.on('toggle_expand', ( ) => {

							itemsCollapsedState = {};

						}, 'expanded')
					);

				}],
				controllerAs: 'proposalsListView'
			};

		}
		])
		.name;
