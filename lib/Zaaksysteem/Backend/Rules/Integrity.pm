package Zaaksysteem::Backend::Rules::Integrity;

use Moose;
use Zaaksysteem::Tools;
use Zaaksysteem::Types qw/Boolean/;

=head1 NAME

Zaaksysteem::Backend::Rules::Integrity - Integrity object.

=head1 SYNOPSIS

    $integrity = Zaaksysteem::Backend::Rules->integrity;

    if ($integrity->success) {
        print "NO PROBLEMS"
    } else {
        for my $problem (@{ $integrity->problems }) {
            print "Rule number: " . $problem->{rule_number} . ', id:' . $problem->{id}
                . ', has problem: ' . $problem->{message};
        }
    }

=head1 DESCRIPTION

Integrity result object containing information about the possible problems with the given
rule listing.

=head1 ATTRIBUTES

=head2 success

isa: boolean

Returns true when integrity looks valid

=cut


has 'success'   => (
    is      => 'rw',
    isa     => Boolean,
);


=head2 failed

The opposite of success

=cut

sub failed {
    return shift->success ? 0 : 1;
}

=head2 problems

isa: ArrayRef

    {
        rule_number => 1,
        id          => undef || 2233,
        message     => 'Email-sjabloon bestaat niet binnen zaaktype',
    }

=cut

has 'problems'  => (
    is      => 'rw',
    isa     => 'ArrayRef',
);

=head2 for_status_number

    my $new_integrity_object = $integrity_object->for_status_number(1);

Returns this object from the perspective of 1 status.

=cut

sub for_status_number {
    my $self            = shift;
    my $status_number   = shift;

    throw(
        'rules/integrity/for_status_number/invalid_status',
        'Invalid status, need a number as first parameter'
    ) unless $status_number && $status_number =~ /^\d+$/;

    my @problems    = grep {
        $_->{status_number} eq $status_number
    } @{ $self->problems };

    return Zaaksysteem::Backend::Rules::Integrity->new(success => (scalar @problems ? 0 : 1), problems => \@problems);
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
