#!/usr/bin/perl -w
use strict;

use Moose;
use Data::Dumper;
use JSON;
use Time::HiRes qw(gettimeofday tv_interval);

use File::Copy;
use File::Temp qw/mktemp/;

use Cwd 'realpath';

use FindBin;
use lib "$FindBin::Bin/../lib";

use Catalyst qw[ConfigLoader];

use Zaaksysteem::Log::CallingLogger;
use Zaaksysteem::Model::DB;
use Zaaksysteem::Model::LDAP;

use Zaaksysteem::Geo;
use Text::CSV;

use Geo::Proj4;
use LWP::UserAgent;



use Config::Any;

my $log = Zaaksysteem::Log::CallingLogger->new();

error("USAGE: $0 [hostname] [configfile] [customer.d-dir] [filename] [collect|load] [optional: dbname]") unless @ARGV && scalar(@ARGV) >= 5;
my ($hostname, $config_file, $config_dir, $filename, $action, $dbname)    = @ARGV;

my $mig_config                              = {};

my $current_hostname;
sub load_customer_d {
    my $config_root = $config_dir;

    # Fetch configuration files
    opendir CONFIG, $config_root or die "Error reading config root: $!";
    my @confs = grep {/\.conf$/} readdir CONFIG;

    info("Found configuration files: @confs");

    for my $f (@confs) {
        my $config      = _process_config("$config_root/$f");
        my @customers   = $config;

        for my $customer (@customers) {
            for my $config_hostname (keys %$customer) {
                my $config_data     = $customer->{$config_hostname};

                if ($hostname eq $config_hostname ) {
                    info('Upgrading hostname: ' . $hostname);
                    $current_hostname = $hostname;
                    $mig_config->{database_dsn}         = $config_data->{'Model::DB'}->{connect_info}->{dsn};
                    $mig_config->{database_password}    = $config_data->{'Model::DB'}->{connect_info}->{password};
                    $mig_config->{ldap_config_basedn}   = $config_data->{LDAP}->{basedn};
                    $mig_config->{customer_info}        = { gemeente => $config_data->{customer_info} };

                    if ($dbname) {
                        $mig_config->{database_dsn} =~ s/dbname=([0-9a-zA-Z_-]+)/dbname=$dbname/;
                    }
                }
            }
        }
    }
}

sub load_zaaksysteem_conf {
    my $config_root = 'etc/customer.d/';
    my $config      = _process_config($config_file);

    $mig_config->{ldap_config_hostname}         = $config->{LDAP}->{hostname};
    $mig_config->{ldap_config_password}         = $config->{LDAP}->{password};
    $mig_config->{ldap_config_user}             = $config->{LDAP}->{admin};
    $mig_config->{ldap_config}                  = $config->{LDAP};
}

sub _process_config {
    my ($config) = @_;
    return Config::Any->load_files({
        files       => [$config],
        use_ext     => 1,
        driver_args => {}
    })->[0]->{$config};
}

load_customer_d();
load_zaaksysteem_conf();

error('Cannot find requested hostname: ' . $hostname) unless $current_hostname;

error('Missing one of required config params: ' . Data::Dumper::Dumper($mig_config))
    unless (
        $mig_config->{database_dsn}
    );

my $dbic = database($mig_config->{database_dsn}, $mig_config->{user}, $mig_config->{database_password});

my ($csv_writer, $csv_fh);

my $collected       = 0;
my $major_fail      = 0;
$dbic->txn_do(sub {
    if ($action eq 'collect') {
        collect_coordinates($dbic);
    } elsif ($action eq 'load') {
        load_coordinates($dbic);
    }

    close($csv_fh) if $csv_fh;
});

$log->info('All done.');
$log->_flush;

sub database {
    my ($dsn, $user, $password) = @_;

    my %connect_info = (
        dsn             => $dsn,
        pg_enable_utf8  => 1,
    );

    $connect_info{password}     = $password if $password;
    $connect_info{user}         = $user if $user;

    Zaaksysteem::Model::DB->config(
        schema_class => 'Zaaksysteem::Schema',
        connect_info => \%connect_info
    );

    my $schema = Zaaksysteem::Model::DB->new->schema;

    $schema->log($log);

    $schema->betrokkene_model->log($log);
    $schema->betrokkene_model->config($mig_config->{customer_info});

    info('Connected to: ' . $dsn);

    return $schema;
}

sub error {
    my $error = shift;

    $log->error($error);
    $log->_flush;

    die("\n");
}

sub info {
    $log->info(sprintf shift, @_);
    $log->_flush;
}

sub load_from_file {
    # Filename already
    my $load_filename;
    if (-f $filename) {
        $load_filename = mktemp('/tmp/collect_csv.XXXX');
        copy($filename, $load_filename);
    } else {
        $load_filename = $filename;
    }
    my $csv = Text::CSV->new ( { binary => 1 } )  # should set binary attribute.
        or error("Cannot use CSV: ".Text::CSV->error_diag());

    open(my $fh, "<:encoding(utf8)", $load_filename) or (info('No CSV found yet'), return ());

    my %coordinates;

    while ( my $row = $csv->getline( $fh ) ) {
        $coordinates{$row->[0]} = $row->[1];
    }

    $csv->eof or $csv->error_diag();
    close $fh;

    return \%coordinates;
}

sub save_to_file {
    my ($identificatie, $coordinates) = @_;

    error('Missing params for save_to_file') unless ($identificatie && $coordinates);

    unless ($csv_writer) {
        $csv_writer = Text::CSV->new ( { binary => 1 } )  # should set binary attribute.
            or error("Cannot use CSV: ".Text::CSV->error_diag());

        $csv_writer->eol ("\r\n");

        open($csv_fh, ">:encoding(utf8)", $filename) or (error('Cannot write to CSV'));
    }

    $csv_writer->print ($csv_fh, [$identificatie, $coordinates]);

    return 1;
}

sub collect_coordinates {
    my $dbic = shift;

    $ENV{DISABLE_ZAAKSYSTEEM_GEO_CODE} = 1;

    my $coordinates = load_from_file();

    my $rs              = $dbic->resultset('BagNummeraanduiding')->search(undef, {order_by => 'me.id'});
    my $count           = $rs->count();

    my $starttime       = [gettimeofday];
    my $entries_done    = 0;

    while(my $entry = $rs->next) {
        $entries_done++;

        if ($major_fail > 10) {
            last;
        }

        info(
            sprintf(
                "Locating coordinates for entry %d of %d (%.3f/second)",
                $entries_done,
                $count,
                $entries_done / tv_interval($starttime, [gettimeofday]),
            )
        );

        load_from_entry($entry, $coordinates);
        load_from_entry($entry->openbareruimte, $coordinates) if $entry->openbareruimte;
    };

    info('Collected: ' . $collected . ' entries');
    if ($major_fail > 10) {
        error('Stopped because google does not respond anymore');
    }
}

sub load_coordinates {
    my $dbic = shift;

    $ENV{DISABLE_ZAAKSYSTEEM_GEO_CODE} = 1;


    my $coordinates = load_from_file();

    my $rs              = $dbic->resultset('BagNummeraanduiding')->search(undef, {order_by => 'me.id'});
    my $count           = $rs->count();

    my $starttime       = [gettimeofday];
    my $entries_done    = 0;
    while(my $entry = $rs->next) {
        $entries_done++;

        if ($coordinates->{ $entry->identificatie }) {
            info(
                sprintf(
                    "Inserting coordinates for entry %d of %d [%s] (%.3f/second)",
                    $entries_done,
                    $count,
                    $entry->geocode_term,
                    $entries_done / tv_interval($starttime, [gettimeofday]),
                )
            );

            $entry->gps_lat_lon($coordinates->{ $entry->identificatie });
            $entry->update;
        }
    };

    $rs              = $dbic->resultset('BagOpenbareruimte')->search(undef, {order_by => 'me.id'});
    $count           = $rs->count();

    $starttime       = [gettimeofday];
    $entries_done    = 0;
    while(my $entry = $rs->next) {
        $entries_done++;

        if ($coordinates->{ $entry->identificatie }) {
            info(
                sprintf(
                    "Inserting coordinates for entry %d of %d [%s] (%.3f/second)",
                    $entries_done,
                    $count,
                    $entry->geocode_term,
                    $entries_done / tv_interval($starttime, [gettimeofday]),
                )
            );

            $entry->gps_lat_lon($coordinates->{ $entry->identificatie });
            $entry->update;
        }
    };
}

sub load_from_entry {
    my $entry       = shift;
    my $cached_coordinates = shift;

    if ($entry->gps_lat_lon) {
        # info ('Skipping ' . $entry->geocode_term . ' because we already loaded it in DB');
        # return;
    }

    if ($cached_coordinates->{ $entry->identificatie }) {
        info ('Skipping ' . $entry->geocode_term . ' because we already loaded it in CSV file');
        return;
    }

    my $location;

    if (!($location    = get_location($entry))) {
        info('Could not find location for: ' . $entry->geocode_term);
    }

    return unless (
        $location->{coordinates} &&
        $location->{coordinates}->{lat} &&
        $location->{coordinates}->{lng}
    );

    $collected++;

    my $coordinates = $location->{coordinates}->{lat} . ',' . $location->{coordinates}->{lng};

    $cached_coordinates->{ $entry->identificatie } = $coordinates;

    save_to_file($entry->identificatie, $coordinates);
}

sub get_location {
    my $entry           = shift;

    info('GO!');
    my $counter = 0;
    my $loc;
    while (!($loc = get_epsg_location($entry)) && $counter < 2) {
        ++$counter;

        info('Sleeping a second');
        sleep 1;
    }

    if ($counter > 2) {
        $major_fail++;
    }

    return $loc;
}

sub get_epsg_location {
    my $entry           = shift;

    my $term            = $entry->geocode_term;
    $term               =~ s/Nederland,//;

    #$term               =~ s/(\d+)([a-zA-Z]+) - (.*)/$1-$2$3/;

    my $xp              = get_xpath_from_term($term);

    my ($street, $number);
    eval {
        $street          = $xp->findvalue('(//xls:Address)[1]/xls:StreetAddress/xls:Street');
        $number          = $xp->findnodes_as_string('(//xls:Address)[1]/xls:StreetAddress/xls:Building');
    };

    if ($@) {
        ### Try again once:
        info('Error retrieving, try again without extra info');
        $term           =~ s/(\d+)[a-zA-Z]* - (.*)/$1/;
        $xp             = get_xpath_from_term($term);

        eval {
            $street          = $xp->findvalue('(//xls:Address)[1]/xls:StreetAddress/xls:Street');
            $number          = $xp->findnodes_as_string('(//xls:Address)[1]/xls:StreetAddress/xls:Building');
        };
    }

    info('Error retrieving: ' . $@) if $@;

    if ($entry->result_source->name eq 'bag_nummeraanduiding' && $number) {
        return format_point($xp->findvalue('(//gml:Point)[1]/gml:pos[1]'));
    }

    if ($entry->result_source->name eq 'bag_openbareruimte' && $street) {
        return format_point($xp->findvalue('(//gml:Point)[1]/gml:pos[1]'));
    }

    return;


}

sub format_point {
    my $point       = shift;

    my ($x, $y)     = split(/ /, $point);


    info('point: ' . $point);
    info('Proj: ' . $x . ',' . $y);

    my $proj = Geo::Proj4->new(init => "epsg:28992");
    my ($lat, $lon) = $proj->inverse($x, $y);

    return {
        'coordinates' => {
            lat     => $lat,
            lng     => $lon,
        }
    };
}


sub get_xpath_from_term {
    my $term            = shift;

    my $ua  = LWP::UserAgent->new;

    my $uri = URI->new('http://geodata.nationaalgeoregister.nl/geocoder/Geocoder');
    $uri->query_form(zoekterm => $term);

    info('Searching for: ' . $term);

    my $xp;
    eval {
        my $res = $ua->get($uri);

        $xp  = XML::XPath->new(xml => $res->content);
    };

    info('Error retrieving: ' . $@) if $@;

    return $xp;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

