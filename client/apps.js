var glob = require('glob'),
	apps;

apps = glob.sync('src/*/index.js', { ignore: 'src/shared' }).map( app => {
	return app.split('/')[1];
});

module.exports = apps;
