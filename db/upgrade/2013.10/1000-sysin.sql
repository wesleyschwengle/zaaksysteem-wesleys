BEGIN;

-- DROP TABLE interface CASCADE;
-- DROP TABLE interface_local_to_remote CASCADE;
-- DROP TABLE remote_api_keys CASCADE;
-- DROP TABLE transaction CASCADE;
-- DROP TABLE transaction_record CASCADE;

CREATE TABLE interface (
	id SERIAL PRIMARY KEY,
	name VARCHAR(40) NOT NULL,
	active BOOLEAN NOT NULL,
	case_type_id INTEGER REFERENCES zaaktype(id),
	max_retries INTEGER NOT NULL,
	interface_config TEXT NOT NULL,
	multiple BOOLEAN default 'f' NOT NULL,
	module TEXT NOT NULL
);

CREATE TABLE interface_local_to_remote (
	id SERIAL PRIMARY KEY,
	local_table VARCHAR(100),
	interface_id INTEGER NOT NULL REFERENCES interface(id),
	local_id INTEGER NOT NULL,
	remote_id VARCHAR(150) NOT NULL
);

CREATE TABLE remote_api_keys (
	id SERIAL PRIMARY KEY,
	key VARCHAR(60) NOT NULL,
	permissions TEXT NOT NULL
);

CREATE TABLE transaction (
	id SERIAL PRIMARY KEY,
	interface_id INTEGER NOT NULL REFERENCES interface(id),
	external_transaction_id VARCHAR(250),
	input_data TEXT,
	input_file INTEGER REFERENCES filestore(id),
	automated_retry_count INTEGER,
	date_created TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
	date_last_retry TIMESTAMP WITHOUT TIME ZONE,
	date_next_retry TIMESTAMP WITHOUT TIME ZONE,
	CONSTRAINT input_data_or_input_file check(input_data IS NOT NULL OR input_file IS NOT NULL)
);

CREATE TABLE transaction_record (
	id SERIAL PRIMARY KEY,
	transaction_id INTEGER REFERENCES transaction(id),
	input TEXT NOT NULL,
	output TEXT NOT NULL,
	is_error BOOLEAN DEFAULT 'f' NOT NULL,
	date_executed TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL
);

COMMIT;
