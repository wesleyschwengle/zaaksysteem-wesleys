/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsUpload', [ 'fileUploader', function ( fileUploader ) {
			
			var supportsFileApi = fileUploader.supports(),
				addEventListener = window.zsFetch('nl.mintlab.utils.events.addEventListener'),
				removeEventListener = window.zsFetch('nl.mintlab.utils.events.removeEventListener');
			
			return {
				scope: true,
				compile: function ( ) {
					
					return function link ( scope, element, attrs ) {
						
						var button,
							form,
							input;
						
						function setUrl ( ) {
							form.attr('action', getUrl());
						}
						
						function getUrl ( ) {
							return attrs.zsUploadUrl || '/filestore/upload';
						}
						
						function setMultiple ( ) {
							var isMultiple = attrs.zsUploadMultiple;
							input.attr('multiple', isMultiple);
						}
						
						function uploadFile ( file ) {
							var upload = fileUploader.upload(file, getUrl());
							
							scope.$emit('upload.start', upload);
							
							upload.subscribe('complete', function ( ) {
								scope.$emit('upload.complete', upload);
							});
							
							upload.subscribe('error', function ( ) {
								scope.$emit('upload.error', upload);
							});
							
							upload.subscribe('end', function ( ) {
								scope.$emit('upload.end', upload);
							});
							
							upload.subscribe('progress', function ( ) {
								scope.$emit('upload.progress', upload);
							});
						}
							
						button = element.find('button');
						if(!button) {
							button = angular.element('<button></button>');
							element.append(button);
						}
						
						form = element.find('form');
						if(!form.length) {
							form = angular.element('<form method="POST" enctype="multipart/form-data"></form>');
							element.append(form);
						}
						
						input = element.find('input');
						if(!input.length) {
							input = angular.element('<input type="file" name="file"/>');
							form.append(input);
						}
						
						attrs.$observe('zsUploadMultiple', function ( ) {
							setMultiple();
						});

						if (attrs.zsUploadUrl === undefined) {
							setUrl();
						} else {
							attrs.$observe('zsUploadUrl', function ( ) {
								setUrl();
							});	
						}
						
						
						
						if(supportsFileApi) {
							
							form.css('display', 'none');
							button.bind('click', function ( ) {
								// reset value to enable upload of same file
								input[0].value = '';
								input[0].click();
							});
							
							// input.bind('change') doesn't work for some reason
							addEventListener(input[0], 'change', function ( ) {
							
								var files = input[0].files,
									i,
									l;
									
								if(files.length) {
									for(i = 0, l = files.length; i < l; ++i) {
										uploadFile(files[i]);
									}
								}
								
							});
							
							if(attrs.ngFileUploadMultiple) {
								input.attr('multiple','multiple');
							}
							
							element.attr('data-zs-file-api-supported', '');
							
						} else {
							(function ( ) {
								
								// replace input element because value is read only in IE
								
								function onChange ( ) {
									var fileName,
										file,
										val;
									
									val = input[0].value.split('\\');
									
									fileName = val[val.length-1];
									
									file = {
										name: fileName,
										form: form[0]
									};
									
									uploadFile(file);
									
									reset();
								}
								
								function initialize ( ) {
									addEventListener(input[0], 'change', onChange);
								}
								
								function reset ( ) {
									
									var clone = input.clone(true),
										// IE barfs if element is null
										before = input[0].previousSibling ? angular.element(input[0].previousSibling) : [];
										
									if(!before.length) {
										input.parent().prepend(clone);
									} else {
										before.after(clone);
									}
									removeEventListener(input[0], 'change', onChange);
									input.remove();
									
									input = clone;
									
									initialize();
								}
								
								initialize();
								
								element.removeAttr('data-zs-file-api-supported');
							})();
						}
						
					};
					
				}
			};
		}]);
	
})();
