package Zaaksysteem::Backend::Sysin::OverheidIO::BAG;
use Moose;

extends 'Zaaksysteem::Backend::Sysin::OverheidIO::Model';

=head1 NAME

Zaaksysteem::Backend::Sysin::OverheidIO::BAG - A model to talk to Overheid.IO BAG

=head1 SYNOPSIS

    use Zaaksysteem::Backend::Sysin::OverheidIO::BAG;
    my $overheidio = Zaaksysteem::Backend::Sysin::OverheidIO::BAG->new(
        key => "your developer key",
    );

=cut

=head1 DESCRIPTION

Implements some builders required by the model.

=head1 METHODS

=head2 _build_type

Builder for the C<type> attribute

=cut

sub _build_type {
    return 'bag';
}

=head2 _build_fieldnames

Builder for the C<fieldnames> attribute.
Returns the field names we expect in the answer from OverheidIO

=cut

sub _build_fieldnames {
    my $self = shift;

    return [qw(
        openbareruimtenaam
        huisnummer
        huisnummertoevoeging
        huisletter
        postcode
        woonplaatsnaam
        gemeentenaam
        provincienaam
        locatie
    )];
}

=head2 _build_queryfields

Builder for the C<queryfields> attribute.
Returns the field names of the fields we want to query on.
Be aware that we return an empty array (ref) due to bugs on the OverheidIO side.
If used you will get a 500 server error.

=cut

sub _build_queryfields {
    my $self = shift;
    return [];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
